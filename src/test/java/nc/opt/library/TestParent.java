package nc.opt.library;

import nc.opt.pages.*;
import net.bytebuddy.implementation.bytecode.Throw;
import org.json.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.sikuli.script.FindFailed;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.aspects.AllureStepsAspects;
import ru.yandex.qatools.allure.events.StepFailureEvent;
import ru.yandex.qatools.allure.events.TestCaseFailureEvent;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * Created by mdelamourd on 07/11/2017.
 */
public class TestParent extends Global {
    //Bibliothèque d'objets
    /*MAIN MENU*/
    public By MAIN_MENU = By.cssSelector("#HomeTabLink");
    public By NAVBAR_SERVICES = By.cssSelector(".navModuleButton[title=\"Service\"]");
    public By NAVBAR_MARKETING = By.cssSelector(".navModuleButton[title=\"Marketing\"]");
    
    /*SUB MENU - RECHERCHE*/
    public By RECHERCHE = module_cssSelector(1,1);
    public By ORGA = module_cssSelector(1,2);
    public By PRIVE = module_cssSelector(1,3);
    public By INCIDENTS = module_cssSelector(2,1);
    public By ACTIVITE = module_cssSelector(2,2);
    public By FILES = module_cssSelector(2,3);
    public By ARTICLES = module_cssSelector(2,4);
    public By FILES2 = module_cssSelector(2,5);
    public By SOUSCRIPTIONS = module_cssSelector(3,1);


    //SELECTION OBJETS DYNAMIQUE
    /*SUB MENU*/
    public By module_cssSelector(int col, int lig) {
        return By.cssSelector("#detailActionGroupControl .nav-tabBody .nav-group:nth-child("+col+")  .nav-subgroup:nth-child("+lig+") a");
    }


    /**************************************************************************************/
    /*                                     Specific                                       */
    /**************************************************************************************/
    public void init() throws IOException {writeFile("src\\test\\resources\\jdd\\jdditer.txt", String.valueOf((0)));}

    @Step
    public void affichageMesure(String mesure, long threshold)  {
        try {
            if (mesure.contains("(")) {
                mesure = ((String) mesure).substring(0, ((String) mesure).indexOf("(")-1);
            }
            long value = Long.valueOf(String.valueOf(Float.valueOf(mesure.split(" ")[mesure.split(" ").length-1].replace("secondes",""))*1000).replace(".0",""));

            if (value > threshold) {
                this.testIsOk = false;
                throw new Exception("Temps d'affichage supérieur à " + threshold/1000 + " sec");
            } else {
                this.testIsOk = true;
            }
        } catch (Exception e) {
            this.testIsOk = false;
            Allure.LIFECYCLE.fire(new StepFailureEvent());
            Allure.LIFECYCLE.fire(new TestCaseFailureEvent());
        }
    }

    @Step
    public int TAG_MESURE(String test, String mesure) throws IOException {
        String json="";
        File file = new File("target\\allure-results");
        file.mkdir();
        if(!testIsOk){mesure="";}
        json = readFileFull("src\\test\\resources\\submodel.txt");
        json=json
                .replace("[DATE_TIME]", LocalDateTime.now().toString())
                .replace("[TEST_NAME]",this.testName)
                .replace("[TEST_STATUS]",testIsOk.toString()).replace("false","ERROR").replace("true", "PASSED")
                .replace("[MESURE_NAME]",this.testName)
                .replace("[MESURE_VALUE]",mesure)
                .replace("[START_TIME]", this.start_test)
                .replace("[END_TIME]", LocalDateTime.now().toString());
        if (json != null) {
            this.jsonResult = this.jsonResult + json;
        }

        return 0;
    }

    /**************************************************************************************/
    /*                              Identification                                        */
    /**************************************************************************************/
    @Step
    public void login() throws Exception {
        try {
            driver.manage().timeouts().pageLoadTimeout(5000, TimeUnit.MILLISECONDS);
            driver.navigate().to(url);
        } catch (Exception e) {
        }
        driver.manage().timeouts().pageLoadTimeout(this.timeoutPage, TimeUnit.MILLISECONDS);
    }

    @Step
    public Long login2() throws InterruptedException, AWTException, FindFailed {
        try {
            driver.manage().timeouts().pageLoadTimeout(5000, TimeUnit.MILLISECONDS);
            driver.navigate().to(url);
        } catch (Exception e) {
        }
        driver.manage().timeouts().pageLoadTimeout(this.timeoutPage, TimeUnit.MILLISECONDS);
        return Long.valueOf(0);
    }

    /**************************************************************************************/
    /*                                Creation Client                                     */
    /**************************************************************************************/
    @Step
    public void afficher_liste_clientsGP() throws Exception {
        clicks.inClick_on(MAIN_MENU, driver);
        clicks.inClick_on(NAVBAR_SERVICES, driver);
        clicks.inClick_on(module_cssSelector(1,3),driver);
        waitsObjects.waitLong(By.cssSelector("li:nth-child(1) a"),driver);
        Assert.assertTrue(driver.findElement(By.cssSelector("li:nth-child(1) a")).isDisplayed());
    }

    @Step
    public void afficher_liste_clientsORGA() throws Exception {
        clicks.inClick_on(MAIN_MENU, driver);
        clicks.inClick_on(NAVBAR_SERVICES, driver);
        clicks.inClick_on(module_cssSelector(1,2),driver);
        assertions.isElementPresent(By.cssSelector("li:nth-child(1) a"),driver,30);
    }

    @Step
    public void afficher_menu_recherche() throws Exception {
        clicks.inClick_on(MAIN_MENU, driver);
        clicks.inClick_on(NAVBAR_SERVICES, driver);
        clicks.inClick_on(module_cssSelector(1,1),driver);
        assertions.isElementPresent(By.cssSelector("#Tabetel_customersearch-main > a > span > span"),driver,30);
    }

    @Step
    public void afficher_formulaire_création() throws Exception {
        String frame =   getIframeVisible();
        clicks.inClick_on(By.cssSelector("li:nth-child(1) a"),driver);
        assertions.isElementContainingTextPresent("SOUMETTRE",By.cssSelector("#crmTopBar li:nth-child(1) span a span"),driver,30);
        sys.switchOnFrame(frame, driver);
        assertions.isElementContainingTextPresent("Données Client Privé (Actif)", By.cssSelector("span.stageNameContent"), driver, 30);
    }

    @Step
    public void creationFormRefCli(String type_client) throws Exception {
        FormGPPage formGPPage = new FormGPPage();
        sys.switchOnFrame(getIframeVisible(), driver);
        sys.pausems(1000);
        Robot rob = new Robot();
        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_type,"Grand public",driver);
        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_type_pi,"Passeport",driver);
        clicks.inClick_on(formGPPage.champs_num_pi,driver);
        sendtext.sendTextJS("0123456789",driver.findElement(formGPPage.champs_num_pi).findElement(By.tagName("input")),driver);

        if (driver.findElement(By.cssSelector(formGPPage.selection_pays_naissance + " div")).getText().toLowerCase().equals("Cliquer pour entrer".toLowerCase())) {
            clicks.inRechercheMenuDeroulantFormulaire(formGPPage.selection_pays_naissance,"AFGHANISTAN",formGPPage.liste_pays_naissance,driver);
        }
        driver.switchTo().defaultContent();
        clicks.inClick_on(formGPPage.bouton_enregistrer,driver);
        sys.switchOnFrame(getIframeVisible(), driver);
        //clicks.inClick_on(formGPPage.bouton_enregistrer_footer,driver);
        sys.pausems(1000);
        clicks.inClick_on(formGPPage.bouton_phase_suivante_actif,driver);
        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_zone,"France DOM TOM",driver);
        clicks.inRechercheMenuDeroulantFormulaire(formGPPage.selection_pays,"FRANCE",formGPPage.liste_pays,driver);
        driver.findElement(By.cssSelector("#header_process_tclab_address1cityid_lookupTable .Lookup_RenderButton_td")).click();
        driver.findElements(By.cssSelector("#header_process_tclab_address1cityid_i_IMenu li")).get(0).click();
        clicks.inRechercheMenuDeroulantFormulaire(formGPPage.selection_codeP, "75019", formGPPage.liste_CP,driver);
        driver.findElements(By.cssSelector("#header_process_optnc_address1_lstpostalcode_i_IMenu li")).get(0).click();

        rob.keyPress(KeyEvent.VK_ENTER);
        rob.keyRelease(KeyEvent.VK_ENTER);
        clicks.inClick_on(By.cssSelector(formGPPage.selection_factu_idem),driver);
        clicks.inClick_on(formGPPage.bouton_phase_suivante_actif,driver);
        sys.pausems(2000);
        clicks.inClick_on(formGPPage.bouton_phase_suivante_actif,driver);
        clicks.inClick_on(By.cssSelector(formGPPage.selection_soumettre),driver);
        driver.switchTo().defaultContent();

        if (!(System.getProperty("test.no_submit").equals("true"))) {
            clicks.inClick_on(formGPPage.bouton_soumettre, driver);
            int  i = 0;
            while (i < 30) {
                try {
                    driver.switchTo().alert();
                    sys.addLog(Lev.INFO,"Confirmation création après " + i + "s");
                    break;
                } catch (Exception e) {
                    sys.pausems(1000);
                    sys.addLog(Lev.INFO,"Attente confirmation création - temps écoulé : " + i + "s");
                    i++;
                }
            }
            Assert.assertTrue(driver.switchTo().alert().getText().equals("La création du client a été soumise avec succès."));
            driver.switchTo().alert().accept();
        } else { //Comportement de contournement en cas de debug pour ne pas griller les Jeux de données clients
            //Fermeture création
            sys.pausems(2000);
            driver.close();
            driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
            sys.addLog(Lev.INFO,"Soumission bypassée - Mode debug");

            //Affichage vue client pour un client hors JDD
            this.nom_client = "BANUKONE";
            this.prenom_client = "JEAN";
            this.type_client = "R";
            this.date_naissance = "05/12/1978";  //A verifier
            Thread.sleep(5000);
            recherche_par_nom(this.nom_client, this.prenom_client, false);
            //selection_client(this.nom_client,this.prenom_client,this.date_naissance);
        }
        sys.addLog(Lev.INFO,"attente vue 360°");
    }

    @Step
    public void creationClientFromScratch(String type_client) throws Exception {
        MainPage mainPage = new MainPage();
        driver.switchTo().defaultContent();
        driver.findElement(mainPage.creation_prive).click();
        sys.pausems(2000);
        FormGPPage formGPPage = new FormGPPage();
        sys.switchOnFrame(getIframeVisible(), driver);
        sys.pausems(1000);
        Robot rob = new Robot();

        String titre;

        if (this.jdd.get("Civilite").equals("MR")) {titre = "Monsieur";
        } else { titre = "Madame"; }

        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_titre,titre,driver);
        clicks.inClick_on(formGPPage.champs_nom_naissance,driver);
        sendtext.sendTextJS(this.jdd.get("Nom"),driver.findElement(formGPPage.champs_nom_naissance).findElement(By.tagName("input")),driver);
        clicks.inClick_on(formGPPage.champs_nom_usage,driver);
        sendtext.sendTextJS(this.jdd.get("Nom"),driver.findElement(formGPPage.champs_nom_usage).findElement(By.tagName("input")),driver);
        clicks.inClick_on(formGPPage.champs_prenom,driver);
        sendtext.sendTextJS(this.jdd.get("Prenom"),driver.findElement(formGPPage.champs_prenom).findElement(By.tagName("input")),driver);
        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_type,"Grand public",driver);
        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_type_pi,"Passeport",driver);
        clicks.inClick_on(formGPPage.champs_num_pi,driver);
        sendtext.sendTextJS("0123456789",driver.findElement(formGPPage.champs_num_pi).findElement(By.tagName("input")),driver);
        clicks.inClick_on(formGPPage.champs_date_naissance,driver);
        sendtext.sendTextJS(this.jdd.get("Date naissance"),driver.findElement(formGPPage.champs_date_naissance).findElement(By.tagName("input")),driver);

        if (driver.findElement(By.cssSelector(formGPPage.selection_pays_naissance + " div")).getText().toLowerCase().equals("Cliquer pour entrer".toLowerCase())) {
            clicks.inRechercheMenuDeroulantFormulaire(formGPPage.selection_pays_naissance,"AFGHANISTAN",formGPPage.liste_pays_naissance,driver);
        }
        rob.keyPress(KeyEvent.VK_ENTER);
        rob.keyRelease(KeyEvent.VK_ENTER);
        sys.pausems(1000);
        driver.switchTo().defaultContent();
        //clicks.inClick_on(formGPPage.bouton_enregistrer,driver);
        sys.switchOnFrame(getIframeVisible(), driver);
        //clicks.inClick_on(formGPPage.bouton_enregistrer_footer,driver);
        sys.pausems(1000);
        clicks.inClick_on(formGPPage.bouton_phase_suivante_actif,driver);
        clicks.inSelectMenuDeroulantFormulaire(formGPPage.selection_zone,"France DOM TOM",driver);
        clicks.inRechercheMenuDeroulantFormulaire(formGPPage.selection_pays,"FRANCE",formGPPage.liste_pays,driver);
        driver.findElement(By.cssSelector("#header_process_tclab_address1cityid_lookupTable .Lookup_RenderButton_td")).click();
        driver.findElements(By.cssSelector("#header_process_tclab_address1cityid_i_IMenu li")).get(0).click();
        clicks.inRechercheMenuDeroulantFormulaire(formGPPage.selection_codeP, "75019", formGPPage.liste_CP,driver);
        driver.findElements(By.cssSelector("#header_process_optnc_address1_lstpostalcode_i_IMenu li")).get(0).click();

        rob.keyPress(KeyEvent.VK_ENTER);
        rob.keyRelease(KeyEvent.VK_ENTER);
        clicks.inClick_on(By.cssSelector(formGPPage.selection_factu_idem),driver);
        clicks.inClick_on(formGPPage.bouton_phase_suivante_actif,driver);
        sys.pausems(2000);
        clicks.inClick_on(formGPPage.bouton_phase_suivante_actif,driver);
        clicks.inClick_on(By.cssSelector(formGPPage.selection_soumettre),driver);
        driver.switchTo().defaultContent();

        if (!(System.getProperty("test.no_submit").equals("true"))) {
            clicks.inClick_on(formGPPage.bouton_soumettre, driver);
            int  i = 0;
            while (i < 30) {
                try {
                    driver.switchTo().alert();
                    sys.addLog(Lev.INFO,"Confirmation création après " + i + "s");
                    break;
                } catch (Exception e) {
                    sys.pausems(1000);
                    sys.addLog(Lev.INFO,"Attente confirmation création - temps écoulé : " + i + "s");
                    i++;
                }
            }
            Assert.assertTrue(driver.switchTo().alert().getText().equals("La création du client a été soumise avec succès."));
            driver.switchTo().alert().accept();
        } else { //Comportement de contournement en cas de debug pour ne pas griller les Jeux de données clients
            //Fermeture création
            sys.pausems(2000);
            driver.close();
            driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
            sys.addLog(Lev.INFO,"Soumission bypassée - Mode debug");

            //Affichage vue client pour un client hors JDD
            this.nom_client = "BANUKONE";
            this.prenom_client = "JEAN";
            this.type_client = "R";
            this.date_naissance = "05/12/1978";  //A verifier
            Thread.sleep(5000);
            recherche_par_nom(this.nom_client, this.prenom_client, false);
            selection_client(this.nom_client,this.prenom_client,this.date_naissance);
        }
        sys.addLog(Lev.INFO,"attente vue 360°");
    }

    /**************************************************************************************/
    /*                                Souscription                                        */
    /**************************************************************************************/

    /**************************************************************************************/
    @Step
    public String ajout_contrat() throws Exception {
        int count = 0;
        VueClientPage vueClientPage = new VueClientPage();
        boolean found = false;
        sys.switchMainFrame(driver);
        clicks.inClick_on(vueClientPage.bouton_ajout_contrat,driver);
        clicks.inClick_on(vueClientPage.bouton_ajout_contrat,driver);
        sys.getSecondWindow(driver);
        driver.manage().window().maximize();

        //Step 1
        Thread.sleep(5000);
        for(WebElement el : driver.findElements(vueClientPage.liste_adresse)){

            if (el.getText().equals(this.adresse_install)) {
                clicks.dragAndDrop(el,driver.findElement(vueClientPage.zone_adresse_install),driver);
                found = true;
                break;
            }
            if ( found ) { break; }
        }

        //Step 2
        Thread.sleep(5000);
        clicks.inClick_on(vueClientPage.workflow_btn2, driver);
        clicks.inClick_on(vueClientPage.workflow_btn2, driver);
        clicks.inClick_on(vueClientPage.recherche_souscription, driver);
        rechercheMenuDepliant(vueClientPage.accordeon_souscription_offre, "Fonis", "Domitel");
        Thread.sleep(5000);
        //Step 3
        clicks.inClick_on(vueClientPage.workflow_btn3, driver);
        Thread.sleep(5000);
        //Step 4
        clicks.inClick_on(vueClientPage.workflow_btn4, driver);
        Thread.sleep(5000);
        //Step 5
        clicks.inClick_on(vueClientPage.workflow_btn5, driver);
        Thread.sleep(5000);
        String ND = choixND();
        Thread.sleep(5000);
        //Step 6
        clicks.inClick_on(vueClientPage.workflow_btn5, driver);
        clicks.inClick_on(vueClientPage.workflow_btn5, driver);
        Thread.sleep(5000);
        choixCompteFactu();
        Thread.sleep(5000);
        //Step 7
        clicks.inClick_on(vueClientPage.workflow_btn6, driver);
        Thread.sleep(5000);

        try {
            if (driver.findElement(By.cssSelector(".text-ser")).getText().equals("Il n’est pas nécessaire de prendre rendez-vous")) {
                sys.addLog(Lev.INFO,"Pas de prise de rendez-vous");
            }
        } catch (Exception e){
            clicks.inClick_on(vueClientPage.sauter_rdv, driver);

            try {
                if (driver.findElement(By.cssSelector(".text-ser")).getText().equals("Il n’est pas nécessaire de prendre rendez-vous")) {
                    sys.addLog(Lev.INFO,"Pas de prise de rendez-vous");
                    clicks.inClick_on(vueClientPage.workflow_btn7, driver);
                }
            } catch (Exception excep){
                clicks.inClick_on(vueClientPage.sauter_rdv, driver);
            }
        }
        //Step 8
        clicks.inClick_on(vueClientPage.workflow_btn7, driver);
        Thread.sleep(10000);
        for (WebElement button : driver.findElements(By.cssSelector(".pull-right-dataActivation button"))) {
            if (button.getText().equals("Confirmer")) {
                button.click();
                button.click();
            }
        }
        Thread.sleep(5000);
        for (WebElement button : driver.findElements(By.cssSelector(".pull-right-dataActivation button"))) {
            if (button.getText().equals("Générer le contrat")) {
                button.click();
            }
        }
        Thread.sleep(10000);
        clicks.inClick_on(By.cssSelector("label[for='printed'] span"),driver);
        Thread.sleep(5000);
        if(!(System.getProperty("test.no_submit").equals("true"))) {
            clicks.inClick_on(By.cssSelector(".action-navbar span:nth-child(3) button.iconLinkButton"), driver);
            Assert.assertTrue(driver.findElement(By.cssSelector(".ebDialogBox‐primaryText")).getText().contains("Commande passée avec succès"));
            clicks.inClick_on(By.cssSelector("button[ng-click='exit()']"), driver);
            driver.close();
            driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
        } else {
            sys.pausems(2000);
            sys.addLog(Lev.INFO,"Soumission bypassée - Mode debug");
        }
       return ND;
    }

    @Step
    public void connectToIWS() throws Exception {
        sendtext.sendSimpleText_selenium("upadmin",driver.findElement(By.cssSelector("#username")));
        sendtext.sendSimpleText_selenium("upadmin",driver.findElement(By.cssSelector("#password")));
        clicks.inClick_on(By.tagName("button"), driver);
        for (WebElement app : driver.findElements(By.className("appIcons-list"))) {
            if (app.getText().equals("Ericsson Order Care")) {
                app.findElement(By.className("ng-binding")).click();
                break;
            }
        }
    }

    /*                            Recherche dans liste options                            */
    /**************************************************************************************/

    public void choixCompteFactu() throws Exception {
        VueClientPage vueClientPage = new VueClientPage();
        clicks.dragAndDrop(driver.findElement(vueClientPage.premier_nd_liste),driver.findElement(vueClientPage.zone_nd_drop),driver);
    }

    @Step
    public String choixND() throws Exception {
        VueClientPage vueClientPage = new VueClientPage();
        clicks.inClick_on(vueClientPage.zone_nd_drop, driver);
        clicks.inClick_on(vueClientPage.zone_nd_drop, driver);
        clicks.inClick_on(vueClientPage.recherche_souscription, driver);
        sys.pausems(3000);
        String ND = driver.findElement(vueClientPage.premier_nd_liste).getText();
        clicks.dragAndDrop(driver.findElement(vueClientPage.premier_nd_liste),driver.findElement(vueClientPage.zone_nd_drop),driver);

        return ND;
    }

    @Step
    public void rechercheMenuDepliant(By tree, String topTitle, String bottomTitle) throws InterruptedException {
        VueClientPage vueClientPage = new VueClientPage();
        WebElement accordeon = driver.findElement(tree);
        for (WebElement el : accordeon.findElements(By.cssSelector("accordion"))) {
            if (el.getText().equals(topTitle)) {
                el.click();
                for (WebElement ele : el.findElements(By.cssSelector("accordion"))) {
                    if (ele.getText().equals(bottomTitle)) {
                        clicks.dragAndDrop(ele,driver.findElement(vueClientPage.zone_offre_drop),driver);
                        break;
                    }
                }
                break;
            }
        }
        Thread.sleep(10000);
    }

    /**************************************************************************************/
    /*                                  Navigation vues                                   */
    /**************************************************************************************/
    @Step
    public String affichage_autres_vue(String vue) throws Exception {
        long startTime = 0;
        long endTime = 0;
        Boolean found = false;
        int retry = 0;
        By checkpoint = By.cssSelector("");
        By checkpoint2 = By.cssSelector("");
        String iframe = "";
        VueClientPage vueClientPage = new VueClientPage();

        try {
            String frame =   getIframeVisible();
            sys.pausems(5000);
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
            driver.switchTo().defaultContent();
            sys.switchOnFrame(frame,driver);
            clicks.inSelectMenuDeroulantMesures(vueClientPage.selection_vue, vueClientPage.liste_vues,vue,driver);
            boolean foundAlert = false;
            //vérfie la présence de la popup
            try {
                driver.switchTo().alert();
                driver.switchTo().alert().dismiss();
                foundAlert = true;
            } catch (NoAlertPresentException Ex) {
                foundAlert = false;
            }

                sys.addLog(Lev.INFO,"début mesure");
            startTime = System.currentTimeMillis();
            while(retry < 10) {
                try {
                    sys.switchMainFrame(driver);
                    sys.switchOnFrame(frame,driver);
                    switch (vue) {
                        case "Fiche Client":
                            checkpoint = By.cssSelector("#gridBodyTable tbody tr");
                            checkpoint2 = By.cssSelector("#lastbilleddate");
                            iframe = "WebResource_IndividualCustomerBillingSummary";
                            if(driver.findElements(checkpoint).size() > 0) {
                                if(driver.findElement(By.cssSelector("#" + iframe)).isDisplayed()) { sys.switchOnFrame(iframe,driver); }
                                if(!driver.findElement(checkpoint2).getText().equals("")) {
                                    found = true;
                                    endTime = System.currentTimeMillis();
                                    sys.addLog(Lev.INFO,"fin mesure");
                                    break;
                                }
                            } else {
                                found = false;
                            }
                            break;
                        case "Facturation et Paiements":
                            checkpoint = By.cssSelector("#gridBodyTable tbody tr");
                            checkpoint2 = By.cssSelector("#lastbilleddate");
                            iframe = "WebResource_IndividualCustomerBillingSummary";
                            if(driver.findElements(checkpoint).size() > 0) {

                                if(driver.findElement(By.cssSelector("#" + iframe)).isDisplayed()) { sys.switchOnFrame(iframe,driver); }
                                if(!driver.findElement(checkpoint2).getText().equals("")) {
                                    found = true;
                                    endTime = System.currentTimeMillis();
                                    sys.addLog(Lev.INFO,"fin mesure, end time :" + endTime);
                                    break;
                                }
                            } else {
                                found = false;
                            }
                            break;
                        case "Suivi des Commandes":
                            checkpoint = By.cssSelector("#btnResume");
                            checkpoint2 = By.cssSelector("#btnCancelOrder");
                            iframe = "WebResource_IndividualCustomerCartGridHtml_opt";
                         if(driver.findElement(By.cssSelector("#" + iframe)).isDisplayed()) { sys.switchOnFrame(iframe,driver); }
                            if(driver.findElement(checkpoint).isDisplayed()) {
                                sys.switchMainFrame(driver);
                                driver.switchTo().frame(getIframeVisible());
                                if(driver.findElement(By.cssSelector("#WebResource_IndividualCustomerOrder")).isDisplayed()) {
                                    driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                                    driver.switchTo().frame("WebResource_IndividualCustomerOrder");
                                }
                                if(driver.findElement(checkpoint2).isDisplayed()) {
                                    found = true;
                                    endTime = System.currentTimeMillis();
                                    sys.addLog(Lev.INFO,"fin mesure, end time :" + endTime);
                                    break;
                                }
                            } else {
                                found = false;
                            }
                            break;
                        case "Informations Complémentaires":
                            checkpoint = By.cssSelector("#grid");
                            if (this.type_client.equals("R") || this.type_client.equals("R")) {
                                iframe = "WebResource_getDoc";
                            } else {
                                iframe = "WebResource_WebResource_getDoc";
                            }
                            if(driver.findElement(By.cssSelector("#" + iframe)).isDisplayed()) { sys.switchOnFrame(iframe,driver); }
                            if(driver.findElement(checkpoint).isDisplayed()) {
                                found = true;
                                endTime = System.currentTimeMillis();
                                sys.addLog(Lev.INFO,"fin mesure");
                                break;
                            } else {
                                found = false;
                            }
                            break;
                        default:
                            break;
                    }
                    if(found) {break;}
                } catch (Exception e) {
                    found = false;
                }
                retry += 1;
                sys.addLog(Lev.INFO,"Tentative de détéction numéro : "+retry);
            }
            driver.manage().timeouts().implicitlyWait(timeoutElem, TimeUnit.MILLISECONDS);
            this.testIsOk = true;
        } catch (Exception e) {
            this.testIsOk = false;
            Allure.LIFECYCLE.fire(new StepFailureEvent());
            Allure.LIFECYCLE.fire(new TestCaseFailureEvent());
        }
        return  "Temps d'affichage de la vue " + vue + " : " + String.valueOf(Float.valueOf((endTime - startTime))/1000) + "secondes || " + String.valueOf(Float.valueOf((endTime - startTime))/1000);
    }

    @Step
    public String affichage_vue_optimo() throws Exception {
        long startTime = 0;
        long endTime = 0;
        int retry = 0;
        String curWin = "";
        Boolean result = false;
        driver.manage().timeouts().implicitlyWait(100,TimeUnit.MILLISECONDS);
        VueClientPage vueClientPage = new VueClientPage();
        String frame = "";
        String stringGrid = "";
        String grid1 = "SubsriptionGrid_divDataArea";//WebResource_individualcustomerrecentinvoices
        String grid2 = "SubscriptionGrid_divDataArea";
        try {
            try {
                driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                if(driver.findElement(By.cssSelector("#" + grid1)).isDisplayed()) { stringGrid = grid1; }
            } catch (Exception e){
                driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                if(driver.findElement(By.cssSelector("#" + grid2)).isDisplayed()) { stringGrid = grid2; }
            }

            clicks.inDoubleClick_onWE(driver.findElements(By.cssSelector("#"+stringGrid+" table tbody tr")).get(0),driver);
            startTime = System.currentTimeMillis();
            sys.addLog(Lev.INFO,"debut mesure");
            driver.manage().timeouts().implicitlyWait(1000,TimeUnit.MILLISECONDS);
            String frameString1 = "WebResource_CustomerSubscriptions";
            String frameString2 = "WebResource_CustomerSubscriptionHTML";
            String stringFrame = "";
            while(retry < 1000) {
                int i = 0;
                try {
                    driver.switchTo().defaultContent();
                    sys.switchOnFrame(getIframeVisible(), driver);
                    try {
                        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
                        if(driver.findElement(By.cssSelector("#" + frameString1)).isDisplayed()) {
                            stringFrame = frameString1;
                            sys.addLog(Lev.INFO,frameString1 + "a bien ete trouvee");
                        }
                    } catch (Exception e){
                        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);

                        if(driver.findElement(By.cssSelector("#" + frameString2)).isDisplayed()) {
                            stringFrame = frameString2;
                            sys.addLog(Lev.INFO,frameString2 + "a bien ete trouvee");
                        }
                    }

                    String frameName = stringFrame;
                    for(WebElement fr : driver.findElements(By.tagName("iframe"))) {
                        if(fr.getAttribute("id").equals(frameName)) {
                            driver.switchTo().frame(i);
                            break;
                        }
                        i++;
                    }

                    waitsObjects.waitMean(By.cssSelector("table.datagrid-btable tbody tr td"),driver);
                    if (driver.findElements(By.cssSelector("table.datagrid-btable tbody tr")).size()>0) {
                        endTime = System.currentTimeMillis();
                        result =true;
                        sys.addLog(Lev.INFO,"Souscriptions affichées");
                        break;
                    }
                } catch (Exception e) {
                    result = false;
                }
                retry += 1;
            }
            driver.manage().timeouts().implicitlyWait(timeoutElem,TimeUnit.MILLISECONDS);
            this.testIsOk = true;
        } catch (Exception e) {
            this.testIsOk = false;
            Allure.LIFECYCLE.fire(new StepFailureEvent());
            Allure.LIFECYCLE.fire(new TestCaseFailureEvent());
        }
        return  "Temps d'affichage détail souscriptions : " + String.valueOf(Float.valueOf((endTime - startTime))/1000) + "secondes || " + String.valueOf(Float.valueOf((endTime - startTime))/1000);
    }


    /**************************************************************************************/
    /*                                  Recherche                                         */
    /**************************************************************************************/
    @Step
    public String recherche_par_nom(String nom, String prenom, boolean check) throws Exception {
        long startTime = 0;
        long endTime = 0;
        int retry = 0;
        Boolean result = false;
        afficher_menu_recherche();
        String frame =   getIframeVisible();

        RechercheIframePage rechercheIframePage = new RechercheIframePage();

        try {
            sys.switchOnFrame(frame, driver);

            clicks.inSelectMenuDeroulant(rechercheIframePage.recherche_selection, "Nom", driver);
            if (!this.type_client.toUpperCase().equalsIgnoreCase("R") && !this.type_client.toUpperCase().equalsIgnoreCase("P")) {
                clicks.inClick_on(rechercheIframePage.orga_clients, driver);
                sendtext.sendTextJS(nom, driver.findElement(rechercheIframePage.champs_raison_sociale),driver);
                driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                sendtext.pressKey_selenium(rechercheIframePage.champs_raison_sociale, Keys.ENTER, driver);
            } else {
                sendtext.sendTextJS(nom, driver.findElement(rechercheIframePage.champs_nom),driver);
                sendtext.sendTextJS(prenom, driver.findElement(rechercheIframePage.champs_prenom),driver);
                driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                sendtext.pressKey_selenium(rechercheIframePage.champs_nom, Keys.ENTER, driver);
            }
            sys.addLog(Lev.INFO,"debut mesure recherche");
            startTime = System.currentTimeMillis();
            while(retry < 100) {
                try {
                    if(driver.findElements(By.cssSelector(".datagrid-row")).size() > 0) {
                        result =true;
                        endTime = System.currentTimeMillis();
                        sys.addLog(Lev.INFO,"fin mesure recherche");
                        break;
                    } else {
                        result = false;
                    }
                    if(result) {break;}
                } catch (Exception e) {
                    result = false;
                }
                retry += 1;
            }
            driver.manage().timeouts().implicitlyWait(timeoutElem, TimeUnit.MILLISECONDS);
            this.testIsOk = true;
        } catch (Exception e) {
            this.testIsOk = false;
            Allure.LIFECYCLE.fire(new StepFailureEvent());
            Allure.LIFECYCLE.fire(new TestCaseFailureEvent());
        }
        return  "Temps de recherche : " + String.valueOf(Float.valueOf((endTime - startTime))/1000) + "secondes || " + String.valueOf(Float.valueOf((endTime - startTime))/1000);
    }

    @Step
    public String recherche_par_nom_direct_auhtentification2(String nom, String prenom, boolean check) throws Exception {
        long startTime = 0;
        long endTime = 0;
        long loginTime = 0;
        int retry = 0;
        Boolean result = false;

        afficher_menu_recherche();
        RechercheIframePage rechercheIframePage = new RechercheIframePage();
        String frame =   getIframeVisible();
        sys.switchOnFrame(frame, driver);

        if (this.env == "PREPROD") {
            sys.switchOnFrame(frame, driver);
        }

        clicks.inSelectMenuDeroulant(rechercheIframePage.recherche_selection, "Nom", driver);

        if (!this.type_client.toUpperCase().equalsIgnoreCase("R") && !this.type_client.toUpperCase().equalsIgnoreCase("P")) {
            clicks.inClick_on(rechercheIframePage.orga_clients, driver);
            sendtext.sendTextJS(nom, driver.findElement(rechercheIframePage.champs_raison_sociale),driver);
            sendtext.pressKey_selenium(rechercheIframePage.champs_raison_sociale, Keys.ENTER, driver);
        } else {
            sendtext.sendTextJS(nom, driver.findElement(rechercheIframePage.champs_nom),driver);
            sendtext.sendTextJS(prenom, driver.findElement(rechercheIframePage.champs_prenom),driver);
            sendtext.pressKey_selenium(rechercheIframePage.champs_nom, Keys.ENTER, driver);
        }

        return  "";
    }

    @Step
    public String recherche_par_nom_direct(String nom, String prenom, boolean check) throws Exception {
        long startTime = 0;
        long endTime = 0;
        long loginTime = 0;
        int retry = 0;
        Boolean result = false;
        String frame =   getIframeVisible();

        afficher_menu_recherche();
        RechercheIframePage rechercheIframePage = new RechercheIframePage();
        sys.switchOnFrame(frame, driver);
        if (this.env == "PREPROD") {sys.switchOnFrame(frame, driver);}
        clicks.inSelectMenuDeroulant(rechercheIframePage.recherche_selection, "Nom", driver);
        if (!this.type_client.toUpperCase().equalsIgnoreCase("R") && !this.type_client.toUpperCase().equalsIgnoreCase("P")) {
            clicks.inClick_on(rechercheIframePage.orga_clients, driver);
            sendtext.sendTextJS(nom, driver.findElement(rechercheIframePage.champs_raison_sociale),driver);
            sendtext.pressKey_selenium(rechercheIframePage.champs_raison_sociale, Keys.ENTER, driver);
        } else {
            sendtext.sendTextJS(nom, driver.findElement(rechercheIframePage.champs_nom),driver);
            sendtext.sendTextJS(prenom, driver.findElement(rechercheIframePage.champs_prenom),driver);
            sendtext.pressKey_selenium(rechercheIframePage.champs_nom, Keys.ENTER, driver);
        }
        sys.addLog(Lev.INFO,"debut mesure");
        startTime = System.currentTimeMillis();
        if(this.env.equals("QUAL")){
            loginTime = login2();
        }
        verifier_chargement_factures();
        return  "Temps de recherche : " + String.valueOf(Float.valueOf((endTime - startTime - loginTime))/1000) + "secondes";
    }

    @Step
    public void selection_client_old(String nom, String prenom, String date_naissance) throws Exception {
        Boolean error = false;
        RechercheIframePage rechercheIframePage = new RechercheIframePage();
        sys.addLog(Lev.INFO,"before if");
        if (System.getProperty("env") == "QUAL") {
            sys.addLog(Lev.INFO,"is in qual");
            try {
                s.wait("img/alert.png",10);
                error = true;
            } catch (FindFailed e) {
                error = false;
            }

            if (error) {
                //login
                Robot nono = new Robot();
                sys.copyPaste(this.login);
                nono.keyPress(KeyEvent.VK_TAB);
                nono.keyRelease(KeyEvent.VK_TAB);
                Thread.sleep(2000);

            //password
            sys.copyPaste(this.password);
            nono.keyPress(KeyEvent.VK_ENTER);
            nono.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(2000);
        } else {
            if (this.type_client == "R" || this.type_client == "P") {
                rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_prive), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            } else {
                rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_orga), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            }
            s.wait("img/alert.png", 10);

                Robot robot = null;
                try { robot = new Robot(); }
                catch (AWTException e) { e.printStackTrace(); }
                sys.pausems(2000);

                //login
                sys.copyPaste(login);
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
                Thread.sleep(2000);

                //password
                sys.copyPaste(password);
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }
        } else {
            if (this.type_client == "R" || this.type_client == "P") {
                rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_prive), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            } else {
                rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_orga), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            }
        }
    }

    @Step
    public long selection_client(String nom, String prenom, String date_naissance) throws Exception {
        Boolean error = false;
        long startTime = 0;
        RechercheIframePage rechercheIframePage = new RechercheIframePage();
        try {
            if (this.type_client == "R" || this.type_client == "P") {
                rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_prive), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            } else {
                rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_orga), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            }
            startTime = System.currentTimeMillis();
            sys.addLog(Lev.INFO,"Début mesure acces fiche client (facture visible) =" + startTime);
            sys.pausems(2000);
            this.testIsOk = true;
        } catch (Exception e) {
            this.testIsOk = false;
            Allure.LIFECYCLE.fire(new StepFailureEvent());
            Allure.LIFECYCLE.fire(new TestCaseFailureEvent());
        }
        return startTime;
    }

    @Step
    public String selection_client2(String nom, String prenom, String date_naissance) throws Exception {
        long startTime = 0;
        long endTime = 0;
        long start2 = 0;
        long end2 = 0;
        long loginTime = 0;
        int retry = 0;
        Boolean result = false;
        String frame =   getIframeVisible();

        WebElement selectionAClicker = driver.findElement(By.xpath("//head"));

        RechercheIframePage rechercheIframePage = new RechercheIframePage();
        Boolean isClientListPresent = driver.findElements(By.xpath("//div[@id=\"SearchResultsGridIndDiv\"]")).size() > 0;
        if (isClientListPresent == true) {
            if(this.type_client == "R" || this.type_client == "P") {
                selectionAClicker = rechercheTableauSelection2(driver.findElement(rechercheIframePage.tableau_recherche_prive), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            } else {
                selectionAClicker = rechercheTableauSelection2(driver.findElement(rechercheIframePage.tableau_recherche_orga), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            }
        }

        clicks.inDoubleClick_onWE(selectionAClicker, driver);
        //start timer
        sys.addLog(Lev.INFO,"debut mesure selection client");
        startTime = System.currentTimeMillis();
        //loginTime = login2();
        while(retry < 100) {
            try {
                driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                long startFrame = System.currentTimeMillis();
                if(driver.findElement(By.cssSelector("#"+frame)).isDisplayed()) { sys.switchOnFrame(frame, driver); }
                if(driver.findElement(By.cssSelector("#WebResource_individualcustomerrecentinvoices")).isDisplayed()) { sys.switchOnFrame("WebResource_individualcustomerrecentinvoices", driver); }
                sys.addLog(Lev.INFO,"frame time "+ (System.currentTimeMillis() - startFrame)+"");
                if(driver.findElements(By.xpath("//table[@id=\"gridBodyTable\"]/tbody")).size() > 0) {
                    result =true;
                    endTime = System.currentTimeMillis();
                    sys.addLog(Lev.INFO,"fin mesure selection client");
                    break;
                } else {
                    result = false;
                }
                if(result) {break;}
            } catch (Exception e) {
                result = false;
            }
            retry += 1;
        }
        driver.manage().timeouts().implicitlyWait(this.timeoutElem, TimeUnit.MILLISECONDS);
        return  "Temps d'affichage de la vue 360 : " + String.valueOf(Float.valueOf(endTime - startTime)/1000) + "secondes";
    }

    @Step
    public String selection_clientDirect(String nom, String prenom, String date_naissance, Boolean isDirect) throws Exception {
        String frame =   getIframeVisible();

        long startTime = 0;
        long endTime = 0;
        long start2 = 0;
        long end2 = 0;
        long loginTime = 0;
        int retry = 0;
        Boolean result = false;
        WebElement selectionAClicker = driver.findElement(By.xpath("//head"));

        RechercheIframePage rechercheIframePage = new RechercheIframePage();
        Boolean isClientListPresent = driver.findElements(By.xpath("//div[@id=\"SearchResultsGridIndDiv\"]")).size() > 0;
        if (isDirect == false && isClientListPresent == true) {
            if(this.type_client == "R" || this.type_client == "P") {
                selectionAClicker = rechercheTableauSelection2(driver.findElement(rechercheIframePage.tableau_recherche_prive), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            } else {
                selectionAClicker = rechercheTableauSelection2(driver.findElement(rechercheIframePage.tableau_recherche_orga), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            }
        } else if(isDirect == false && isClientListPresent == true) {
            if(this.type_client == "R" || this.type_client == "P") {
                selectionAClicker = rechercheTableauSelection2(driver.findElement(rechercheIframePage.tableau_recherche_prive), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            } else {
                selectionAClicker = rechercheTableauSelection2(driver.findElement(rechercheIframePage.tableau_recherche_orga), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            }
        }

        if (isDirect == true){
            loginTime = login2();
        } else {
            clicks.inDoubleClick_onWE(selectionAClicker, driver);
            //start timer
            sys.addLog(Lev.INFO,"debut mesure");
            driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
            startTime = System.currentTimeMillis();
            loginTime = login2();
            while(retry < 100) {
                try {
                    start2 = System.currentTimeMillis();
                    if(driver.findElement(By.cssSelector("#"+frame)).isDisplayed()) { sys.switchOnFrame(frame, driver); }
                    if(driver.findElement(By.cssSelector("#WebResource_individualcustomerrecentinvoices")).isDisplayed()) { sys.switchOnFrame("WebResource_individualcustomerrecentinvoices", driver); }
                    end2 = System.currentTimeMillis();
                    if(driver.findElements(By.xpath("//table[@id=\"gridBodyTable\"]/tbody")).size() > 0) {
                        result =true;
                        endTime = System.currentTimeMillis();
                        sys.addLog(Lev.INFO,"fin mesure");
                        break;
                    } else {
                        result = false;
                    }
                    if(result) {break;}
                } catch (Exception e) {
                    result = false;
                }
                retry += 1;

            }
        }
        driver.manage().timeouts().implicitlyWait(this.timeoutElem, TimeUnit.MILLISECONDS);
        return  String.valueOf(Float.valueOf((endTime - startTime - loginTime - (end2-start2)))/1000);
    }

    @Step
    public boolean selection_client_creation(String nom, String prenom, String date_naissance) throws Exception {
        Boolean error = false;
        try {
            s.wait("img/alert.png",10);
            error = true;
        } catch (FindFailed e) {
            sys.addLog(Lev.INFO,"Le client n'existe pas - création en cours");
            error = false;
        }

        try {
            VueClientPage vueClientPage = new VueClientPage();
            driver.findElement(vueClientPage.bouton_ajout_contrat);
            error = true;
        } catch (Exception e) {
            sys.addLog(Lev.INFO,"Le client n'existe pas - création en cours");
            error = false;
        }

        if (error) {
            sys.addLog(Lev.INFO,"Client existant détécté");
            sys.addLog(Lev.INFO,"Etape création client annulée - Poursuite scénario -> Création adresse -> Création contrat");
            //login
            Robot nono = new Robot();
            sys.copyPaste(this.login);
            nono.keyPress(KeyEvent.VK_TAB);
            nono.keyRelease(KeyEvent.VK_TAB);
            Thread.sleep(2000);

            //password
            sys.copyPaste(this.password);
            nono.keyPress(KeyEvent.VK_ENTER);
            nono.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(2000);
            //Assert.assertTrue(1 == 0);
        } else {
            RechercheIframePage rechercheIframePage = new RechercheIframePage();
            rechercheTableauSelection(driver.findElement(rechercheIframePage.tableau_recherche_refcli), nom.toUpperCase(), prenom.toUpperCase(), date_naissance.toUpperCase());
            sys.getSecondWindow(driver);
            sys.pausems(1000);
            driver.manage().window().maximize();
        }

        return error;
    }

    public void change_adresse_lockField_text(String css, String value) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].setAttribute('title', '"+value+"')", driver.findElement(By.cssSelector(css)));
        js.executeScript("document.querySelector('"+css+" div span').innerHTML = '"+ value +"';");
        js.executeScript("arguments[0].setAttribute('defaultvalue', '"+value+"')", driver.findElement(By.cssSelector(css + "_i")));

    }
    public void change_lockField_text_sig(String css, String value) throws Exception{
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement  element_optnc_sigid_i = driver.findElement(By.cssSelector("#optnc_sigid_i"));
        WebElement element_div_parent_sigid_i = driver.findElement(By.cssSelector("#optnc_sigid div[class='ms-crm-Inline-Edit']"));
        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent_sigid_i);
        js.executeScript("arguments[0].setAttribute('controlmode', 'display')", element_optnc_sigid_i);
     //   js.executeScript("arguments[0].setAttribute('defaultvalue', '"+value+"')", element_optnc_sigid_i);
      //  js.executeScript("arguments[0].setAttribute('display', 'block')", element_optnc_sigid_i);
     //   sys.pausems(2000);
        element_div_parent_sigid_i.click();
        sys.pausems(1000);
        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent_sigid_i);
        sys.pausems(1000);
        element_div_parent_sigid_i.click();
        sys.pausems(1000);
        element_div_parent_sigid_i.sendKeys(value);
        sys.pausems(1000);

        element_div_parent_sigid_i.sendKeys(Keys.ENTER);
     //   js.executeScript("document.querySelector('"+css+" div label div').innerHTML = 'ID Sig "+ value +"';");
       // js.executeScript("arguments[0].setAttribute('title', '"+value+"')", driver.findElement(By.cssSelector(css)));
       // js.executeScript("document.querySelector('"+css+" div span').innerHTML = '"+ value +"';");
      //  js.executeScript("arguments[0].setAttribute('defaultvalue', '"+value+"')", driver.findElement(By.cssSelector(css + "_i")));
    }

    public void change_lockField_text(String css, String value) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement  element_i = driver.findElement(By.cssSelector(css+"_i"));
        WebElement element_div_parent = driver.findElement(By.cssSelector(css+" div[class='ms-crm-Inline-Edit']"));

        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent);
        js.executeScript("arguments[0].setAttribute('controlmode', 'display')", element_i);
        sys.pausems(1000);
        element_div_parent.click();
        sys.pausems(1000);
        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent);
        sys.pausems(1000);
        element_div_parent.click();
        sys.pausems(1000);
        element_div_parent.sendKeys(value);
        element_div_parent.sendKeys(Keys.ENTER);
        element_div_parent.sendKeys(Keys.ENTER);
    }


    public void change_lockField_text_Pays(String css, String value)  throws Exception {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement  element = driver.findElement(By.cssSelector(css+"_lookupTable"));
        WebElement element_div_parent = driver.findElement(By.cssSelector(css + " div[class='ms-crm-Inline-Edit ms-crm-Inline-Lookup ms-crm-Inline-HasError']"));

        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent);
        js.executeScript("arguments[0].setAttribute('controlmode', 'display')", element);
        sys.pausems(1000);
        element_div_parent.click();
        sys.pausems(1000);
        element_div_parent.sendKeys(value);
        element_div_parent.sendKeys(Keys.ENTER);
        element_div_parent.sendKeys(Keys.ENTER);
    }
    public void change_lockField_text_LookupTable(String css, String value) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement  element = driver.findElement(By.cssSelector(css+"_lookupTable"));
        WebElement element_div_parent = driver.findElement(By.cssSelector(css + " div[class='ms-crm-Inline-Edit ms-crm-Inline-Lookup']"));
        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent);
        js.executeScript("arguments[0].setAttribute('controlmode', 'display')", element);
        sys.pausems(1000);
        element_div_parent.click();
        sys.pausems(1000);
        element_div_parent.sendKeys(value);
        element_div_parent.sendKeys(Keys.ENTER);
        element_div_parent.sendKeys(Keys.ENTER);
    }
    public void change_lockField_text_LookupTable_v2(String css, String value) throws Exception {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement  element = driver.findElement(By.cssSelector(css+"_lookupTable"));
        WebElement element_div_parent = driver.findElement(By.cssSelector(css + " div[class='ms-crm-Inline-Edit ms-crm-Inline-Lookup']"));
        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent);
      //  js.executeScript("arguments[0].setAttribute('controlmode', 'display')", element);
        sys.pausems(1000);
        element.click();
        sys.pausems(1000);
        js.executeScript("arguments[0].setAttribute('style', 'display:block;')", element_div_parent);
        element.click();
        element_div_parent.sendKeys(value);
        element_div_parent.sendKeys(Keys.ENTER);
        element_div_parent.sendKeys(Keys.ENTER);
    }


    public void change_adresse_lockField_lookfor(String css, String value) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].setAttribute('title', '"+value+"')", driver.findElement(By.cssSelector(css + " div span:nth-child(0)")));
        js.executeScript("document.querySelector('"+css+" div span:nth-child(0)').innerHTML = 'N° et nom de voie "+ value +"';");
        js.executeScript("document.querySelector('"+css+" div span:nth-child(1)').innerHTML = '"+ value +"';");
        js.executeScript("arguments[0].setAttribute('defaultvalue', '"+value+"')", driver.findElement(By.cssSelector(css + "_i")));

    }

    @Step
    public void ajout_adresse_install(String adresse) throws Exception {
        VueClientPage vueClientPage = new VueClientPage();
        String curWin = driver.getWindowHandle();
        sys.switchMainFrame(driver);
        clicks.inClick_on(vueClientPage.bouton_creer_adresse, driver);
        sys.pausems(5000);
        sys.getSecondWindow(driver);
        driver.manage().window().maximize();
        CreationAdressPage creationAdressPage = new CreationAdressPage();
        sys.switchOnFrame("contentIFrame0",driver);
        clicks.inClick_on(creationAdressPage.type_adresse_selection,driver);
        clicks.inClick_on(creationAdressPage.adresse_install,driver);
        clicks.inClick_on(creationAdressPage.adresse_zone,driver);
        clicks.inClick_on(creationAdressPage.zone_nc,driver);
        sys.switchMainFrame(driver);
    //    sys.switchOnFrame("contentIFrame0",driver);
     //   JavascriptExecutor js = (JavascriptExecutor)driver;
        Robot nono = new Robot();
        sys.pausems(5000);
        nono.keyPress(KeyEvent.VK_ESCAPE);

      /*    sendtext.sendTextJS(adresse, driver.findElement(creationAdressPage.refloc_champs_adresse),driver);
        clicks.inClick_on(creationAdressPage.refloc_bouton_recherche,driver);
        clicks.inClick_on(creationAdressPage.point_adresse,driver);
        driver.manage().timeouts().pageLoadTimeout(1000,TimeUnit.MILLISECONDS);
        clicks.inClick_onWithSkip(creationAdressPage.selection_desserte,driver);
        driver.manage().timeouts().pageLoadTimeout(this.timeoutPage,TimeUnit.MILLISECONDS);
        String[] imgs = new String[7];
        imgs[0]="img/desserte1.png";
        imgs[1]="img/desserte2.png";
        imgs[2]="img/desserte3.png";
        imgs[3]="img/desserte4.png";
        imgs[4]="img/desserte5.png";
        imgs[5]="img/desserte6.png";
        imgs[6]="img/desserte7.png";
        Boolean found = false;*/

   //     for(int z = 0 ; z < 3 ; z++){
          //  sys.pausems(15000);
           /* for (String img : imgs){
                try {
                    s.setRect(431,376,1055,449);
                    s.click(img);
                    driver.manage().timeouts().implicitlyWait(1000,TimeUnit.MILLISECONDS);
                    if (driver.findElement(creationAdressPage.choix_desserte).isDisplayed()) {
                        driver.manage().timeouts().implicitlyWait(this.timeoutElem,TimeUnit.MILLISECONDS);
                        found = true;
                        break;
                    }
                } catch (Exception e){
                    sys.addLog(Lev.INFO,"L'image " + img + "n'as pas été trouvée");
                    sys.addLog(Lev.INFO,e);
                    found = false;
                }
            }
            if(found) {
                clicks.inDoubleClick_on(creationAdressPage.choix_desserte,driver);
                clicks.inClick_on(creationAdressPage.save_close,driver);*/
                sys.switchMainFrame(driver);
                sys.switchOnFrame("contentIFrame0",driver);

                change_lockField_text("#etel_street1",this.refloc_adresse);

                sys.pausems(3000);
                change_lockField_text_sig("#optnc_sigid",this.jdd.get("Identifiant SIG"));

                sys.pausems(3000);
                change_lockField_text_Pays("#etel_countryid",this.refloc_pays);
                sys.pausems(3000);
                change_lockField_text("#optnc_province",this.refloc_province);
                change_lockField_text("#optnc_street4",this.refloc_lieudit);
                change_lockField_text("#optnc_reflocid",this.reflocid);
                change_lockField_text_LookupTable_v2("#optnc_lstpostalcode",this.refloc_CP);
                sys.pausems(3000);

                change_lockField_text_LookupTable_v2("#ete" + "l_cityid",this.refloc_localite);

        //change_lockField_text("#optnc_commune",this.refloc_ville);

        // Req req = new Req();
              //  req.sendFormRequest();

             //   clicks.inClick_on(creationAdressPage.bouton_enregistrer, driver);
                sys.switchMainFrame(driver);
                if (!(System.getProperty("test.no_submit").equals("true"))) {
                    clicks.inClick_on(creationAdressPage.bouton_soumettre, driver);
                    //sys.pausems(10000);
                    waits.waitAndSkipAlert((long)30000, driver);
                    //nono.keyPress(KeyEvent.VK_ENTER);
                    sys.pausems(5000);
                    driver.switchTo().window(driver.getWindowHandles().toArray(new String[0])[0]);
                } else { //Comportement de contournement en cas de debug pour ne pas griller les Jeux de données clients
                    //Fermeture création
                    sys.pausems(2000);
                    sys.addLog(Lev.INFO,"Soumission bypassée - Mode debug");
                    driver.close();
                    sys.pausems(2000);
                    driver.switchTo().alert().accept();
                    driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
                }
              //  break;
         //   } else {
            //    clicks.inClick_on(creationAdressPage.zoomOut, driver);
          //  }
      //  }
    }

    /**************************************************************************************/
    /*                               Vérifications                                        */
    /**************************************************************************************/
    @Step
    public void verifier_chargement_accueil() throws Exception {
        afficher_menu_recherche();
        MainPage mainPage = new MainPage();
        Assert.assertTrue(driver.findElement(mainPage.menu).isDisplayed());
        Assert.assertTrue(driver.findElement(mainPage.champs_recherche).isDisplayed());
        Assert.assertTrue(driver.findElement(mainPage.bouton_recherche).isDisplayed());
        Assert.assertTrue(driver.findElement(mainPage.champs_utilisateur).isDisplayed());
        Assert.assertTrue((this.usr_name + ", " + this.usr_firstname).equalsIgnoreCase(driver.findElement(mainPage.champs_utilisateur).getText()));
    }

    @Step
    public void verifier_chargement_creation_client() {

    }

    @Step
    public void verifier_chargement_360() throws Exception {
        try {
            verifier_chargement_adresses();
            this.testIsOk = true;
        } catch (Exception e) {
            this.testIsOk = false;
            Allure.LIFECYCLE.fire(new StepFailureEvent());
            Allure.LIFECYCLE.fire(new TestCaseFailureEvent());
        }
    }

    @Step
    public long[] verifier_chargement_factures() throws InterruptedException {
        int retry = 0;
        long startTime = 0;
        long endTime = 0;
        Boolean result = false;
        long timeSwitchFrame = 0;
        long startFrame = 0;
        long endFrame =0;
        long endDispo = 0;
        long[] res = new long[2];
        VueClientPage vueClientPage = new VueClientPage();
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        String grid1 = "SubsriptionGrid_divDataArea";//WebResource_individualcustomerrecentinvoices
        String grid2 = "SubscriptionGrid_divDataArea";
        String stringGrid = "";
        while(retry < 100) {
            int i = 0;
            try {
                String frame =   getIframeVisible();
                startTime = System.currentTimeMillis();
                if(driver.findElement(By.cssSelector("#"+frame)).isDisplayed()) {
                    sys.switchOnFrame(frame, driver);
                }
                waitsObjects.waitMeanClickable(vueClientPage.selection_vue,driver);
                endDispo = System.currentTimeMillis();
                try {
                    driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                    if(driver.findElement(By.cssSelector("#" + grid1)).isDisplayed()) { stringGrid = grid1; }
                } catch (Exception e){
                    driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                    if(driver.findElement(By.cssSelector("#" + grid2)).isDisplayed()) { stringGrid = grid2; }
                }

                if(driver.findElements(By.cssSelector("#"+stringGrid+" table tbody tr")).size() > 0) {  ////table[@id="gridBodyTable"]/tbody
                    result =true;
                    Assert.assertTrue(driver.findElements(By.cssSelector("#"+stringGrid+" table tbody tr")).size() > 0); ////table[@id="gridBodyTable"]/tbody
                    endTime = System.currentTimeMillis();
                    sys.addLog(Lev.INFO,"fin mesure acces fiche client (Souscriptions visibles) = " + endTime);
                    break;
                } else {
                    result = false;
                    driver.switchTo().defaultContent();
                }
                if(result) {break;}
            } catch (Exception e) {
                result = false;
                driver.switchTo().defaultContent();
            }
            retry += 1;
        }
        driver.manage().timeouts().implicitlyWait(timeoutElem, TimeUnit.MILLISECONDS);
        res[0] = endTime;
        res[1] = endDispo;
        return res;
    }

    @Step
    public void verification_ligne_active(String ND) throws InterruptedException {
        int retry = 0;
        long startTime = 0;
        long endTime = 0;
        Boolean result = false;
        long timeSwitchFrame = 0;
        long startFrame = 0;
        long endFrame =0;

        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        String frameString1 = "WebResource_CustomerSubscriptionHTML";//WebResource_individualcustomerrecentinvoices
        String frameString2 = "WebResource_IndividualCustomerSubscription";
        String stringFrame = "";
        while(retry < 181) {
            int i = 0;
            try {
                String frame =   getIframeVisible();
                startTime = System.currentTimeMillis();
                if(driver.findElement(By.cssSelector("#"+frame)).isDisplayed()) {
                    sys.switchOnFrame(frame, driver);
                }
                try {
                    driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                    if(driver.findElement(By.cssSelector("#" + frameString1)).isDisplayed()) { stringFrame = frameString1; }
                } catch (Exception e){
                    driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
                    if(driver.findElement(By.cssSelector("#" + frameString2)).isDisplayed()) { stringFrame = frameString2; }
                }
                String frameName = stringFrame;
                for(WebElement fr : driver.findElements(By.tagName("iframe"))) {
                    if(fr.getAttribute("id").equals(frameName)) {
                        driver.switchTo().frame(i);
                        break;
                    }
                    i++;
                }
                if(driver.findElements(By.xpath("//table/tbody")).size() > 0) {  ////table[@id="gridBodyTable"]/tbody
                    Assert.assertTrue(driver.findElements(By.xpath("//table/tbody")).size() > 0); ////table[@id="gridBodyTable"]/tbody
                    for (WebElement line : driver.findElements(By.xpath("//table/tbody/tr"))) {
                        if(line.getText().contains(ND)){
                            Assert.assertTrue(line.getText().contains("ACTIVE62"));
                            result = true;
                            break;
                        }
                    }
                } else {
                    result = false;
                    driver.switchTo().defaultContent();
                }
                if(result) {break;}
            } catch (Exception e) {
                result = false;
                driver.switchTo().defaultContent();
            }
            retry += 1;
            driver.navigate().refresh();
            Thread.sleep(30000);
            driver.switchTo().defaultContent();
        }
        driver.manage().timeouts().implicitlyWait(timeoutElem, TimeUnit.MILLISECONDS);
    }

    @Step
    public void verifier_chargement_souscriptions() throws InterruptedException {

    }

    @Step
    public void verifier_chargement_incidents(){

    }

    @Step
    public void verifier_chargement_adresses() throws Exception {
        int timeout = 0;
        int interval = 500;
        String frame =   getIframeVisible();
        driver.manage().timeouts().implicitlyWait(1000,TimeUnit.MILLISECONDS);
        int dur = 0;
        boolean found = false;
        while (dur < 60 && found == false) {
            try {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(frame);
                driver.findElement(By.xpath("//*[@id=\"Addreses_divDataArea\"]/div/table/tbody"));
                sys.addLog(Lev.INFO,"tableau trouvé");
                found = true;
            } catch (Exception e) {
                sys.addLog(Lev.INFO,"tableau non trouvé");
            }
        }
        driver.manage().timeouts().implicitlyWait(timeoutElem, TimeUnit.MILLISECONDS);
        WebElement tableau_adresse = driver.findElement(By.xpath("//*[@id=\"Addreses_divDataArea\"]/div/table/tbody"));

        Assert.assertTrue(tableau_adresse.isDisplayed());
        while(tableau_adresse.findElements(By.tagName("tr")).size() == 0 && timeout < 20000){
            sys.pausems(interval);
            timeout = timeout + interval;
        }
        Assert.assertTrue(tableau_adresse.findElements(By.tagName("tr")).size() > 0);
        sys.addLog(Lev.INFO, "La vue 360° a bien été chargée");
    }


    /**************************************************************************************/
    /*                                    Tableaux                                        */
    /**************************************************************************************/
    public void rechercheTableauSelection(WebElement tableau, String texte1, String texte2, String texte3) throws Exception {
        java.util.List<WebElement> lignes = tableau.findElements(By.tagName("tr"));
        java.util.List<WebElement> cells = tableau.findElements(By.tagName("td"));
        WebElement webElementToClick = driver.findElement(By.xpath("//head"));
        Boolean found1 = false;
        Boolean found2 = false;

        try {
            for (WebElement ligne : lignes){
                for(WebElement cell : ligne.findElements(By.tagName("td"))){
                    //if (this.env.equals("QUAL")) {
                    if(cell.findElement(By.tagName("div")).getText().equals((texte1 + " " + texte2).toUpperCase() ) || cell.findElement(By.tagName("div")).getText().equals((texte1).toUpperCase())){
                        found1 = true;
                    }
                    /*} else {
                        if(cell.findElement(By.tagName("div")).getText().equals((texte1 + " " + texte2)) || cell.findElement(By.tagName("div")).getText().equals(texte2.toUpperCase())){
                            found1 = true;
                        }
                    }*/
                    if(cell.findElement(By.tagName("div")).getText().equals(texte3)){
                        found2 = true;
                    }
                    if(found1 && found2){
                        clicks.inDoubleClick_onWE(ligne.findElements(By.tagName("td")).get(1),driver);
                        break;
                    }
                }
                if(found1 && found2){
                    found1 = false;
                    found2 = false;
                    break;
                }
            }
        } catch (Exception e) {

        }
    }

    public WebElement rechercheTableauSelection2(WebElement tableau, String texte1, String texte2, String texte3) throws Exception {
        java.util.List<WebElement> lignes = tableau.findElements(By.tagName("tr"));
        java.util.List<WebElement> cells = tableau.findElements(By.tagName("td"));

        WebElement webElementToClick = driver.findElement(By.xpath("//head"));
        Boolean found1 = false;
        Boolean found2 = false;

        try {
            for (WebElement ligne : lignes){
                for(WebElement cell : ligne.findElements(By.tagName("td"))){
                    if(cell.findElement(By.tagName("div")).getText().equals((texte1 + " " + texte2).toUpperCase())){
                        found1 = true;
                    }
                    if(cell.findElement(By.tagName("div")).getText().equals(texte3)){
                        found2 = true;
                    }
                    if(found1 && found2){
                        webElementToClick = ligne.findElements(By.tagName("td")).get(1);
                        break;
                    }
                }
                if(found1 && found2){
                    found1 = false;
                    found2 = false;
                    break;
                }
            }
        } catch (Exception e) {

        }
        return webElementToClick;
    }
}
