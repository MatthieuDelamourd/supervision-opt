package nc.opt.library;

import com.opencsv.CSVReader;
import nc.opt.actions.*;
import nc.opt.checks.Assertions;
import nc.opt.checks.WaitsObjects;
import org.json.JSONObject;
import org.monte.media.Format;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.By;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.sikuli.script.Screen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.model.Attachment;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.monte.media.AudioFormatKeys.EncodingKey;
import static org.monte.media.AudioFormatKeys.FrameRateKey;
import static org.monte.media.AudioFormatKeys.KeyFrameIntervalKey;
import static org.monte.media.AudioFormatKeys.MIME_AVI;
import static org.monte.media.AudioFormatKeys.MediaType;
import static org.monte.media.AudioFormatKeys.MediaTypeKey;
import static org.monte.media.AudioFormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.*;

/**
 * Created by mdelamourd on 13/11/2017.
 */
public class Global {
    //Environnement
    public WebDriver driver;
    public static String driverPath = "drivers\\";
    public String url;
    public String browser;
    public ScreenRecorder recorder;
    public Boolean testIsOk;
    public String testName;
    public String jsonResult;
    public String start_test;
    public LocalDateTime start_timestamp;
    public JSONObject jsonPretty;

    //Dataset
    public String client;
    public String type_client;
    public String nom_client;
    public String prenom_client;
    public ArrayList<Map<String, String>> jdds = new ArrayList<>();
    public Map<String, String > jdd = new HashMap<String,String >();
    public String env;
    public ArrayList<String> keys;
    public String date_naissance;
    public String lieu_naissance_full;
    public String lieu_naissance_pays;
    public String lieu_naissance_ville;
    public String no_adresse;
    public String nom_rue;
    public String adr_ville;
    public String reflocid;
    public String refloc_adresse;
    public String refloc_ville;
    public String refloc_lieudit;
    public String refloc_pays;
    public String refloc_localite;
    public String refloc_CP;
    public String refloc_province;
    public String adresse_install;

    //User
    public String user;
    public String usr_name;
    public String usr_firstname;
    public String login = "";
    public String password = "";

    //Liraries
    public Screen s;
    public Clipboard clipboard;
    public StringSelection selection;
    public Robot robot;
    public Logger logger;
    public Clicks clicks;
    public Sendtext sendtext;
    public Waits waits;
    public WaitsObjects waitsObjects;
    public MouseMoves mousemoves;
    public Sys sys;
    public Assertions assertions;
    public long timeoutPage;
    public long timeoutElem;
    public enum Lev{
        DEBUG,
        INFO,
        WARN,
        ERR,
        TRACE,
    }
    public String jddfile = System.getProperty("test.jdd");

    public Global(){}

    public void startVideo(int x, int y, int w, int h) throws IOException, AWTException {
        //Create a instance of GraphicsConfiguration to get the Graphics configuration
        //of the Screen. This is needed for ScreenRecorder class.
        GraphicsConfiguration gc = GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getDefaultScreenDevice()
                .getDefaultConfiguration();

        //Create a instance of ScreenRecorder with the required configurations
        recorder = new ScreenRecorder(gc,
                new Rectangle(x,y,w,h),                                                                             //ScreenArea
                new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),                                    //fileFormat
                new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,   //screenFormat
                        CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                        DepthKey, (int)24, FrameRateKey, Rational.valueOf(15),
                        QualityKey, 1.0f,
                        KeyFrameIntervalKey, (int) (15 * 60)),
                new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey,"black",                                  //mouseFormat
                        FrameRateKey, Rational.valueOf(30)),
                null,//audioFormat
                new File("target/videoCap/")                           //videoPath
                );
    }

    public void setUp() throws IOException, AWTException {
        this.start_timestamp = LocalDateTime.now();
        this.start_test = LocalDateTime.now().toString();
        testIsOk = false;
        try {
            if(!this.getClass().toString().contains("nc.opt.selenium.creation")) {
               writeFile("src\\test\\resources\\jdd\\jdditer.txt", String.valueOf((0)));
           }

           this.jdd = getNextJdd();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.env = System.getProperty("test.env");
        this.browser = System.getProperty("test.browser");
        this.user = System.getProperty("test.user");

        try {
            this.url = getBaseUrl(this.env);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.client = this.jdd.get("Nom");
        this.nom_client = this.client;
        this.prenom_client = this.jdd.get("Prenom");
        this.type_client = this.jdd.get("Type Client");
        this.date_naissance = this.jdd.get("Date naissance");
        this.lieu_naissance_full = this.jdd.get("Lieu naissance");
        if (this.lieu_naissance_full.split(" ").length >= 2) {
            this.lieu_naissance_ville = this.lieu_naissance_full.split(" ")[0].trim();
            this.lieu_naissance_pays = this.lieu_naissance_full.split(" ")[1].trim();
        } else {
            this.lieu_naissance_ville = "";
            this.lieu_naissance_pays = "";
        }
        this.no_adresse = this.jdd.get("No dans la voie");
        this.nom_rue = this.jdd.get("Nom de la voie");

        this.adr_ville = this.jdd.get("Ville");
        this.adresse_install = this.no_adresse + " " + this.nom_rue;
        this.timeoutElem = 30000;
        this.timeoutPage = 30000;
        try {
            this.login = getLogin(this.user);
            this.password = getPassword(this.user);
            this.usr_name = getUsrName(this.user);
            this.usr_firstname = getUsr_firstname(this.user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("************************************************************************");
        System.out.println("Starting tests with params : csv file -> " + this.jddfile);
        System.out.println("Environnement : " + this.env );
        System.out.println("User : " + this.usr_firstname.toLowerCase() + " " + this.usr_name.toLowerCase() +" -> creds :" + this.login + "/" + this.password);
        System.out.println("url : " + this.url);
        System.out.println("client : " + this.nom_client + " " + this.prenom_client +" de type : "+this.type_client.toUpperCase());
        System.out.println("Date et lieu de naissance : " + this.date_naissance + " " + this.lieu_naissance_ville + " " + this.lieu_naissance_pays);
        System.out.println("Adresse d'installation : " + this.adresse_install);
        System.out.println("************************************************************************");

        if (this.browser == "chrome") {
            System.out.println("Démarrage de chrome");
            System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
            DesiredCapabilities capa = DesiredCapabilities.chrome();
            driver = new ChromeDriver(capa);
        } else if (this.browser.equals("ie")) {
                System.out.println("Démarrage d'internet explorer");
                System.setProperty("webdriver.ie.driver", driverPath+"IEDriverServer.exe");
                DesiredCapabilities capa = DesiredCapabilities.internetExplorer();
                capa.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true);
                capa.setCapability(InternetExplorerDriver.LOG_LEVEL, "ERROR");
                capa.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
                capa.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
                capa.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, UnexpectedAlertBehaviour.ACCEPT);
                capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                capa.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                capa.setJavascriptEnabled(true);
                driver = new InternetExplorerDriver(capa);
        } else if (this.browser.equals("server")) {
            System.out.println("Démarrage d'internet explorer");
            System.setProperty("webdriver.ie.driver", driverPath+"IEDriverServer.exe");
            DesiredCapabilities capa = DesiredCapabilities.internetExplorer();
            /*capa.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true);
            capa.setCapability(InternetExplorerDriver.LOG_LEVEL, "ERROR");
            capa.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
            capa.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
            capa.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, UnexpectedAlertBehaviour.ACCEPT);
            capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            capa.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            capa.setJavascriptEnabled(true);*/
            driver = new RemoteWebDriver(new URL("http://0.0.0.0:5566/wd/hub"),capa);
        } else {
            System.out.println("Démarrage d'internet explorer");
            System.setProperty("webdriver.ie.driver", driverPath+"IEDriverServer.exe");
            DesiredCapabilities capa = DesiredCapabilities.internetExplorer();
            capa.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true);
            capa.setCapability(InternetExplorerDriver.LOG_LEVEL, "ERROR");
            capa.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
            capa.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
            capa.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, UnexpectedAlertBehaviour.ACCEPT);
            capa.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            capa.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            capa.setJavascriptEnabled(true);
            driver = new InternetExplorerDriver(capa);
        }
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(this.timeoutElem, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(this.timeoutPage, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        s = new Screen();

        logger = LoggerFactory.getLogger(Global.class);
        clicks = new Clicks();
        sendtext = new Sendtext();
        waits = new Waits();
        mousemoves = new MouseMoves();
        sys = new Sys();
        assertions = new Assertions();
        waitsObjects = new WaitsObjects();
        try {
            Map<String,String> ref = new HashMap<String, String>();
            ref = sys.getRefLocInfo(this.no_adresse, this.nom_rue, this.adr_ville);
            this.reflocid = ref.get("reflocid");
            this.refloc_adresse = ref.get("refloc_adresse");
            this.refloc_ville = ref.get("refloc_ville");
            this.refloc_lieudit = ref.get("refloc_lieudit");
            this.refloc_pays = ref.get("refloc_pays");
            this.refloc_localite = ref.get("refloc_localite");
            this.refloc_CP = ref.get("refloc_CP");
            this.refloc_province = ref.get("refloc_province");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (System.getProperty("test.capture").equals("true")) {
            startVideo(driver.manage().window().getPosition().getX(),driver.manage().window().getPosition().getY(), driver.manage().window().getSize().getWidth(), driver.manage().window().getSize().getHeight() );
            recorder.start();
        }

        //Navigation to URL
        try {
            driver.manage().timeouts().pageLoadTimeout(5000, TimeUnit.MILLISECONDS);
            driver.navigate().to(url);
        } catch (Exception e) {
        }
        driver.manage().timeouts().pageLoadTimeout(this.timeoutPage, TimeUnit.MILLISECONDS);

        //initiateJson();
    }

    @ru.yandex.qatools.allure.annotations.Attachment()
    public byte[] video_attachement (File video) {
        return video.getAbsolutePath().getBytes();
    }

    public void tearDown() throws IOException {
        File film = null;
        System.out.println("****************************TEARDOWN****************************************");
        edit_json();
        System.out.println("--------------------------JSON CREATED--------------------------------------");
        if (System.getProperty("test.capture").equals("true")) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            recorder.stop();
            List<File> createdMovieFiles = (List<File>) recorder.getCreatedMovieFiles();
            for(File movie : createdMovieFiles){
                System.out.println("New movie created: " + movie.getAbsolutePath());
                if (this.testIsOk) {
                    File file = new File(movie.getAbsolutePath());
                    if(file.delete()){
                        System.out.println("TEST IS OK "+ movie.getAbsolutePath() + "File deleted");
                    }else {
                        System.out.println("TEST IS OK BUT MOVIE NOT FOUND - DELETION CANCELED");
                    }
                } else {
                    video_attachement(movie);
                }
            }
        }
        try {
            System.out.println("Closing IE browser");
            driver.quit();
            System.out.println("DRIVER CLOSED");
        } catch (Exception e) {
            System.out.println("UNABLE TO CLOSE THE CURRENT DRIVER. IE IS STILL OPENED. WILL BE CLOSED AT NEXT EXEC");
        }
        System.out.println("\n\n\n\n\n\n\n\n");
    }

    public void edit_json() throws IOException {
        String json = readFileFull("src\\test\\resources\\model.txt");
        json=json
                .replace("[SECTION_REPLACE]", jsonResult)
                .replace(",}}","}}")
                .replace("null","")
                .replace("\n","")
                .trim();
        this.jsonPretty = new JSONObject(json);
        writeFile("target\\allure-results\\"+this.getClass().getSimpleName()+".json",jsonPretty.toString());
        writeFile("C:\\Dev\\RESULTAT\\json\\"+this.getClass().getSimpleName()+".json",jsonPretty.toString());
        writeFile("C:\\xampp\\htdocs\\RESULTAT\\json\\"+this.getClass().getSimpleName()+".json",jsonPretty.toString());
    }

    /********************************************/
    /*                PARAMS                    */
    /********************************************/
    @Step
    public String Environnement(String env) {
        return System.getProperty("test.env");
    }

    public String getBaseUrl(String environnement) throws IOException {
        String baseUrl="";
        ArrayList<Map<String, String>> users = readCSV("src\\test\\resources\\jdd\\env.csv");
        for (Map<String, String> usr : users) {
            if(usr.get("Environnement").equals(environnement)) {
                baseUrl = usr.get("baseurl");
                break;
            }
        }
        return baseUrl;
    }

    public String getLogin(String user) throws IOException {
        String login="";
        ArrayList<Map<String, String>> users = readCSV("src\\test\\resources\\jdd\\users.csv");
        for (Map<String, String> usr : users) {
            if(usr.get("profil").equals(user)) {
                login = usr.get("login");
                break;
            }
        }
        return login;
    }

    public String getPassword(String user) throws IOException {
        String password = "";
        ArrayList<Map<String, String>> users = readCSV("src\\test\\resources\\jdd\\users.csv");
        for (Map<String, String> usr : users) {
            if(usr.get("profil").equals(user)) {
                password = usr.get("password");
                break;
            }
        }
        return password;
    }

    public String getIframeVisible() throws Exception{

        String frame;
        driver.switchTo().defaultContent();
        try {
            driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);

            driver.findElement(By.cssSelector("#contentIFrame1")).isDisplayed();
            frame = "contentIFrame1";
        }
       catch(Exception e) {
            frame = "contentIFrame0";
        }
        driver.manage().timeouts().implicitlyWait(timeoutElem, TimeUnit.MILLISECONDS);
        return frame;
    }

    public String getUsrName(String user) throws IOException {
        String username="";
        ArrayList<Map<String, String>> users = readCSV("src\\test\\resources\\jdd\\users.csv");
        for (Map<String, String> usr : users) {
            if(usr.get("profil").equals(user)) {
                username = usr.get("nom");
                break;
            }
        }
        return username;
    }

    public String getUsr_firstname(String user) throws IOException {
        String firstname="";
        ArrayList<Map<String, String>> users = readCSV("src\\test\\resources\\jdd\\users.csv");
        for (Map<String, String> usr : users) {
            if(usr.get("profil").equals(user)) {
                firstname = usr.get("prenom");
                break;
            }
        }
        return firstname;
    }

    /********************************************/
    /*                   JDD                    */
    /********************************************/
    public Map<String, String> getJddById(int id) throws IOException {
        return readCSV(jddfile).get(id);
    }

    public Map<String, String> getNextJdd() throws IOException {
        ArrayList<Map<String, String>> jdd = readCSV(jddfile);
        long len = jdd.size();
        Integer currentId;
        if(readFile("src\\test\\resources\\jdd\\jdditer.txt") == "") {
            currentId = 0;
        } else {
            currentId =  Integer.valueOf(readFile("src\\test\\resources\\jdd\\jdditer.txt"));
        }

        if (currentId == len-1) {
            writeFile("src\\test\\resources\\jdd\\jdditer.txt", String.valueOf((0)));
        } else {
            writeFile("src\\test\\resources\\jdd\\jdditer.txt", String.valueOf((currentId + 1)));
        }
        return jdd.get(currentId);
    }

    /*********************************************/
    /*             Json processing               */
    /*********************************************/
    public void initiateJson() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        String json = readFile("C:\\Dev\\supervision-opt\\src\\test\\resources\\model.txt");
        String jsonSubModel = readFile("C:\\Dev\\supervision-opt\\src\\test\\resources\\Submodel.txt");
        json.replace("[DATETIME]",timeStamp);
        jsonSubModel.replace("[REP-TEST-ID]", this.getClass().getName());
        json.replace("[REP-SUBMODEL1]",jsonSubModel);
        writeFile("C:\\Dev\\supervision-opt\\target\\results.json",json);
    }


    /********************************************/
    /*                 Files I/O                */
    /********************************************/
    public void writeFile(String file, String texte) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter (new FileOutputStream(file), "utf-8"));
        writer.write(texte);
        writer.close();
    }

    public String readFile(String file) throws IOException {
        String text = "";

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        while ((line = reader.readLine()) != null) {
            text = line;
        }
        return text;
    }

    public String readFileFull(String file) throws IOException {
        String text = "";

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        while ((line = reader.readLine()) != null) {
            text = text + "\n" + line;
        }
        return text;
    }

    public ArrayList<Map<String, String>> readCSV(String csvFile) throws IOException {
        CSVReader reader = null;
        reader = new CSVReader(new FileReader(csvFile));
        String[] line;
        int i;
        int count = 0;
        Map<String,String> jddEl = new HashMap<String,String>();
        ArrayList<Map<String,String>> jdd = new ArrayList<Map<String,String>>();
        this.keys = new ArrayList<String>();
        line = reader.readNext();
        for (String id : line) {
            jddEl.put(id,"");
            this.keys.add(id);
        }

        while ((line = reader.readNext()) != null) {
            i = 0;
            for (String key : this.keys) {
                jddEl.put(key,line[i]);

                i = i + 1;
            }
            jdd.add(jddEl);
            jddEl = new HashMap<String,String>();
            count = count + 1;
        }
        return jdd;
    }
}
