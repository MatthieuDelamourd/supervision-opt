package nc.opt.selenium.runner;

import nc.opt.library.TestParent;
import org.testng.annotations.Test;

import java.io.*;

import static java.util.logging.Logger.global;

/**
 * Created by mdelamourd on 29/11/2017.
 */
public class InitTest  extends TestParent{
    @Test
    public void InitTest() throws IOException {
            if (System.getProperty("test.val")!=null) {
                sys.addLog(Lev.INFO,"Initialisation au jeux de donnees " + System.getProperty("test.val"));
                writeFile("src\\test\\resources\\jdd\\jdditer.txt",System.getProperty("test.val"));
            } else {
                sys.addLog(Lev.INFO,"Aucun jeux de donnee specifie, Reinitialisation du JDD à 0");
                writeFile("src\\test\\resources\\jdd\\jdditer.txt","0");
            }
    }
}
