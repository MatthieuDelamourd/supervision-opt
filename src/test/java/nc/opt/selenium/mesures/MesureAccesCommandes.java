package nc.opt.selenium.mesures;

import nc.opt.library.TestParent;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Method;


/**
 * Created by mdelamourd on 26/10/2017.
 */
@Features("Mesure de Performance")
public class MesureAccesCommandes extends TestParent {
    @Title("Affichage vue Commandes depuis la vue 360° - Prive peu de souscriptions")
    @Test()
    @Stories("Mesure Accès Commandes")
    public void MesureVueCommandesPriveFaible() throws Exception {
        System.out.println("******************** Test : " + this.getClass().getName() + " ********************");
        Environnement(this.env);
        this.nom_client = "JAMOT";
        this.prenom_client = "";
        this.type_client = "R";
        this.date_naissance = "03/01/1974";
        Thread.sleep(5000);
        recherche_par_nom(this.nom_client, this.prenom_client, false);
        this.prenom_client = "OLIVIER";
        selection_client(this.nom_client,this.prenom_client,this.date_naissance);
        verifier_chargement_360();
        String mesure = affichage_autres_vue("Suivi des Commandes");
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Affichage vue commandes", mesure.substring(mesure.indexOf("||")+3));
    }

    @Title("Affichage vue commandes depuis la vue 360° - Orga souscriptions nombreuses")
    @Test()
    @Stories("Mesure Accès Commandes")
    public void MesureVueCommandesOrgaFort() throws Exception {
        System.out.println("******************** Test : " + this.getClass().getName() + " ********************");
        Environnement(this.env);
        this.nom_client = "MUNICIPALITE MONT DORE";
        this.prenom_client = "";
        this.type_client = "E";
        this.date_naissance = "";
        Thread.sleep(5000);
        recherche_par_nom(this.nom_client, this.prenom_client, false);
        //selection_client(this.nom_client,this.prenom_client,this.date_naissance);
        verifier_chargement_360();
        String mesure = affichage_autres_vue("Suivi des Commandes");
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Affichage vue commandes", mesure.substring(mesure.indexOf("||")+3));
    }

    @BeforeMethod
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterMethod
    public void tearDown() throws IOException { super.tearDown(); }

    @BeforeMethod
    public void setUp(Method testContext) throws IOException, AWTException {
        this.testName = testContext.getName();
    }
}
