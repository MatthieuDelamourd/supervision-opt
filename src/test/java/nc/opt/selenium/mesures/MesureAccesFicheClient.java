package nc.opt.selenium.mesures;

import nc.opt.library.TestParent;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Method;


/**
 * Created by mdelamourd on 26/10/2017.
 * Modified by pmariet on 11/12/2017.
 */
@Features("Mesure de Performance")

public class MesureAccesFicheClient extends TestParent {

    @Title("Temps d’acces synthese fiche client (apres recherche manuelle) - Client prive - charge faible")
    @Stories("Mesure Accès Fiche Client")
    @Test()
    public void accesFicheClientPriveFaible() throws Exception {
        System.out.println("******************** Test : " + this.getClass().getName() +" accesFicheClientPriveFaible() "+ " ********************");
        Environnement(this.env);
        this.nom_client = "JAMOT";
        this.prenom_client = "";
        this.type_client = "R";
        this.date_naissance = "03/01/1974";
        Thread.sleep(5000);
        recherche_par_nom(this.nom_client, this.prenom_client, false);
        this.prenom_client = "OLIVIER";
        long tDoubleClick = selection_client(this.nom_client,this.prenom_client,this.date_naissance);
        long[] res = new long[2];
        res = verifier_chargement_factures();
        String mesure1 = "Temps d'affichage de la vue 360 : " + String.valueOf(Float.valueOf(res[0]-tDoubleClick)/1000) + "secondes (Page utilisable après : "+String.valueOf(Float.valueOf(res[1]-tDoubleClick)/1000)+")";
        affichageMesure(mesure1, 30000);
    }

    @Title("Temps d’acces direct synthese fiche client (apres recherche manuelle) - Client prive - charge faible")
    @Test()
    @Stories("Mesure Accès Fiche Client")
    public void accesDirectFicheClientPriveFaible() throws Exception {
        System.out.println("******************** Test : " + this.getClass().getName() +"accesDirectFicheClientPriveFaible()"+ " ********************");
        Environnement(this.env);
        this.nom_client = "JAMOT";
        this.prenom_client = "OLIVIER";
        this.type_client = "R";
        this.date_naissance = "03/01/1974";
        Thread.sleep(5000);
        String rech = recherche_par_nom(this.nom_client, this.prenom_client, false);
        long startTime = System.currentTimeMillis();
        long[] res = new long[2];
        res = verifier_chargement_factures();
        long rechTime = Long.valueOf(String.valueOf(Float.valueOf(rech.split(" ")[rech.split(" ").length-1].replace("secondes",""))*1000).replace(".0",""));
        String mesure = "Temps d'affichage de la vue 360 : " + String.valueOf(Float.valueOf(res[0]-startTime)/1000) + "secondes (Page utilisable après : "+String.valueOf(Float.valueOf(res[1]-startTime)/1000)+") || " + String.valueOf(Float.valueOf((res[0] - startTime))/1000);
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Affichage fiche client", mesure.substring(mesure.indexOf("||")+3));
    }

    @Title("Temps d’acces direct synthese fiche client (apres recherche manuelle) - Client pro - charge forte")
    @Test()
    @Stories("Mesure Accès Fiche Client")
    public void accesDirectFicheClientOrgaForte() throws Exception {
        System.out.println("******************** Test : " + this.getClass().getName() +"accesDirectFicheClientOrgaForte() " +" ********************");
        Environnement(this.env);
        this.nom_client = "MUNICIPALITE MONT DORE";
        this.prenom_client = "";
        this.type_client = "E";
        this.date_naissance = "";
        Thread.sleep(5000);
        String rech = recherche_par_nom(this.nom_client, this.prenom_client, false);
        long startTime = System.currentTimeMillis();
        long[] res = new long[2];
        res = verifier_chargement_factures();
        long rechTime = Long.valueOf(String.valueOf(Float.valueOf(rech.split(" ")[rech.split(" ").length-1].replace("secondes",""))*1000).replace(".0",""));
        String mesure = "Temps d'affichage de la vue 360 : " + String.valueOf(Float.valueOf(res[0]-startTime)/1000) + "secondes (Page utilisable après : "+String.valueOf(Float.valueOf(res[1]-startTime)/1000)+") || " + String.valueOf(Float.valueOf((res[0] - startTime))/1000);
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Affichage fiche client", mesure.substring(mesure.indexOf("||")+3));
    }

    @BeforeMethod
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterMethod
    public void tearDown() throws IOException { super.tearDown(); }

    @BeforeMethod
    public void setUp(Method testContext) throws IOException, AWTException {
        this.testName = testContext.getName();
    }
}
