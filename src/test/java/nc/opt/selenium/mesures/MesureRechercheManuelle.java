package nc.opt.selenium.mesures;
import nc.opt.library.TestParent;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Created by mdelamourd on 26/10/2017.
 */
@Features("Mesure de Performance")
public class MesureRechercheManuelle extends TestParent {
    @Title("Recherche manuelle de fiches clients - Liste 1 a 2 clients prives")
    @Test()
    @Stories("Recherche manuelle")
    public void MesureRechercheManuelleFaiblePrive() throws Exception {
        //Init
        System.out.println("******************** Test : " + this.getClass().getName() + " ********************");
        Environnement(this.env);

        //Data set
        this.nom_client = "BANUKONE";
        this.prenom_client = "";
        this.type_client = "R";
        Thread.sleep(2000);

        //Measurement
        String mesure = recherche_par_nom(this.nom_client, this.prenom_client, false);
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Recherche manuelle", mesure.substring(mesure.indexOf("||")+3));
    }

    @Title("Recherche manuelle de fiches clients - Liste > 40 clients orga")
    @Test()
    @Stories("Recherche manuelle")
    public void MesureRechercheManuelleMoyenneOrga() throws Exception {
        //Init
        System.out.println("******************** Test : " + this.getClass().getName() + " ********************");
        Environnement(this.env);

        //Data set
        this.nom_client = "a*";
        this.prenom_client = "";
        this.type_client = "E";
        Thread.sleep(2000);
        String mesure = recherche_par_nom(this.nom_client, this.prenom_client, false);
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Recherche manuelle", mesure.substring(mesure.indexOf("||")+3));
    }

    @Title("Recherche manuelle de fiches clients - 1 client RefCli")
    @Stories("Recherche manuelle")
    @Test()
    public void MesureRechercheManuelleRefCli() throws Exception {
        //Init
        System.out.println("******************** Test : " + this.getClass().getName() + " ********************");
        Environnement(this.env);

        //Data set
        this.nom_client = "CONSTANT";
        this.prenom_client = "LOUIS";
        this.type_client = "R";
        Thread.sleep(2000);

        //Measurement
        String mesure = recherche_par_nom(this.nom_client, this.prenom_client, false);
        affichageMesure(mesure.substring(0,mesure.indexOf("||")-1), 30000);
        TAG_MESURE("Recherche manuelle", mesure.substring(mesure.indexOf("||")+3));
    }

    @BeforeMethod
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterMethod
    public void tearDown() throws IOException { super.tearDown(); }

    @BeforeMethod
    public void setUp(Method testContext) throws IOException, AWTException {
        this.testName = testContext.getName();
    }
}