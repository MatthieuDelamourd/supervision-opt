package nc.opt.selenium.creation;

import nc.opt.library.TestParent;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Method;


/**
 * Created by mdelamourd on 26/10/2017.
 */
@Features("Mesure de Performance")

public class SouscriptionE2ETest extends TestParent {

    @Title("Creation client - Ajout adresse - Souscription")
    @Test()
    @Stories("Souscription bout en bout")

    public void SouscriptionE2ETest() throws Exception {
        Environnement(this.env);
        long startCreation = System.currentTimeMillis();
        login();

        recherche_par_nom("JAMOT", "", true);
        Boolean error = false; //selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);
        if (!error) {
            creationClientFromScratch(this.type_client);//<-
            Thread.sleep(2000);
        }

        verifier_chargement_360();
        ajout_adresse_install(this.adresse_install);//<-
        long endCreation = System.currentTimeMillis();
        Thread.sleep(10000);
        long startContrat = System.currentTimeMillis();
        String ND = ajout_contrat();//<-
        driver.close();
        driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
        verifier_chargement_360();
        long endContrat = System.currentTimeMillis();

        //Verification ligne active (jusqu'a 30mn!!!)
        //long startActivation = System.currentTimeMillis();
        driver.switchTo().defaultContent();

        //recherche_par_nom(this.nom_client, this.prenom_client, true);
        //selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);
        //verification_ligne_active(ND);
        //long endActivation = System.currentTimeMillis();

        affichageMesure("Duree creation client : " + String.valueOf(Float.valueOf(endCreation - startCreation)/1000), 3600000 );
        affichageMesure("Duree souscription : " + String.valueOf(Float.valueOf(endContrat - startContrat)/1000), 3600000 );
        //affichageMesure("Duree activation ligne : " + String.valueOf(Float.valueOf(endActivation - startActivation)/1000), 3600000 );
        affichageMesure("Duree total cycle attente : " + String.valueOf(Float.valueOf(endCreation - startCreation + endContrat - startContrat)/1000), 3600000 );
    }
    @BeforeClass
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterClass
    public void tearDown() throws IOException { super.tearDown(); }

    @BeforeMethod
    public void setUp(Method testContext) throws IOException, AWTException {
        this.testName = testContext.getName();
    }
}
