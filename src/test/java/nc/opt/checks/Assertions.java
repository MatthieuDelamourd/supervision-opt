package nc.opt.checks;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by mdelamourd on 15/11/2017.
 */
public class Assertions extends Object {
    public void Assertions() {}

    public void checkTextIs(String text, By selector, WebDriver driver) {
        Assert.assertEquals(text, driver.findElement(selector).getText());
    }

    public void isElementPresent(By selector, WebDriver driver, long timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        w.until(ExpectedConditions.presenceOfElementLocated(selector));
        Assert.assertTrue(driver.findElement(selector).isDisplayed());
    }

    public void isElementContainingTextPresent(String text, By selector, WebDriver driver, long timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        w.until(ExpectedConditions.textToBe(selector, text));
        Assert.assertTrue(driver.findElement(selector).isDisplayed());
    }

    public void isElementWithAttributePresent(String attribute, String value, By selector, WebDriver driver, long timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        w.until(ExpectedConditions.attributeContains(selector, attribute, value));
        Assert.assertTrue(driver.findElement(selector).isDisplayed());
    }

    public void isElementNotPresent(By selector, WebDriver driver, long timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        w.until(ExpectedConditions.invisibilityOfElementLocated(selector));
    }
}
