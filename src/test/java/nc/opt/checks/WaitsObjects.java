package nc.opt.checks;

import nc.opt.library.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.testng.Assert;

/**
 * Created by mdelamourd on 15/11/2017.
 */
public class WaitsObjects extends Global {
    public void WaitsObjects() {}

    /**************************************************************************************/
    /*                              Attente affichage element                             */
    /**************************************************************************************/
    public void waitShort(By cssSelector, WebDriver driver) {
        WebDriverWait attente = new WebDriverWait(driver, 2);
        attente.until(ExpectedConditions.visibilityOfElementLocated(cssSelector));
        System.out.println("VERIF : L'élément -> " + cssSelector.toString() + " a bien été trouvé");
    }

    public void waitMean(By cssSelector, WebDriver driver) {
        WebDriverWait attente = new WebDriverWait(driver, 30);
        attente.until(ExpectedConditions.visibilityOfElementLocated(cssSelector));
        System.out.println("VERIF : L'élément -> " + cssSelector.toString() + " a bien été trouvé");
    }

    public void waitMeanClickable(By cssSelector, WebDriver driver) {
        WebDriverWait attente = new WebDriverWait(driver, 30);
        attente.until(ExpectedConditions.elementToBeClickable(cssSelector));
        System.out.println("VERIF : L'élément -> " + cssSelector.toString() + " a bien été trouvé et est cliquable");
    }

    public boolean retryingFindClick(By by) {
        boolean result = false;
        int attempts = 0;
        while(attempts < 5) {
            try {
                Assert.assertTrue(driver.findElement(by).isDisplayed());
                result = true;
                break;
            } catch(Exception e) {
            }
            attempts++;
        }
        return result;
    }

    public void waitLong(By cssSelector, WebDriver driver) {
        WebDriverWait attente = new WebDriverWait(driver, 60);
        attente.until(ExpectedConditions.visibilityOfElementLocated(cssSelector));
        System.out.println("VERIF : L'élément -> " + cssSelector.toString() + " a bien été trouvé");
    }

    public void waitImage(String img, long timeout) throws FindFailed {
        s.wait(img, timeout);
    }

    public void waitImages(String[] imgs, long timeout) throws FindFailed {
        for (String img : imgs){
            s.wait(img,timeout/imgs.length);
        }
    }
}
