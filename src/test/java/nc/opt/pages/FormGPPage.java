package nc.opt.pages;

import nc.opt.library.Global;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mdelamourd on 13/11/2017.
 */
public class FormGPPage extends Global {
    public By bouton_enregistrer;
    public By bouton_enregistrer_footer;
    public By bouton_enregistrer_fermer;
    public By bouton_fermer;
    public By bouton_soumettre;
    public By bouton_phase_suivante_actif;
    public By bouton_phase_suivante_inactif;

    public By champs_nom_naissance;
    public By champs_nom_usage;
    public By champs_prenom;
    public By champs_num_pi;
    public By champs_date_naissance;
    public By champs_recherche_pays_naissance;
    public By champs_recherche_lieu_naissance;

    public String selection_titre;
    public String selection_type;
    public String selection_priorite;
    public String selection_capacite;
    public String selection_type_pi;
    public String selection_soumettre;
    public String selection_zone;
    public String selection_pays;
    public String selection_pays_naissance;
    public String selection_recherche_localite;
    public String selection_codeP;
    public String selection_factu_idem;

    public By liste_pays;
    public By liste_pays_naissance;
    public By liste_localite;
    public By liste_CP;

    public String title;

    public FormGPPage() {
        this.bouton_soumettre = By.cssSelector("#crmTopBar li:nth-child(1) span a span");
        this.bouton_enregistrer = By.cssSelector("#crmTopBar li:nth-child(2) span a span");
        this.bouton_enregistrer_footer = By.cssSelector(".inline-block.save-status-icon.status-icon");
        this.bouton_enregistrer_fermer = By.cssSelector("#crmTopBar li:nth-child(3) span a span");
        this.bouton_phase_suivante_inactif=By.cssSelector("#stageAdvanceActionContainer");
        this.bouton_phase_suivante_actif=By.cssSelector("#stageAdvanceActionContainer");

        this.champs_nom_naissance=By.cssSelector("#header_process_optnc_usagelastname");
        this.champs_nom_usage=By.cssSelector("#header_process_tclab_lastname");
        this.champs_prenom=By.cssSelector("#header_process_tclab_firstname");
        this.champs_num_pi=By.cssSelector("#header_process_tclab_iddocumentnumber");
        this.champs_date_naissance=By.cssSelector("#header_process_tclab_dateofbirth");
        this.champs_recherche_pays_naissance=By.cssSelector("#header_process_optnc_countryofbirth");
        this.champs_recherche_lieu_naissance=By.cssSelector("#header_process_optnc_placeofbirth1");

        this.selection_titre = "#header_process_optnc_titlecode";
        this.selection_type="#header_process_optnc_customersegment";
        this.selection_priorite="#header_process_optnc_priorityofintervention";
        this.selection_capacite="#header_process_optnc_customerlegalcapacity";
        this.selection_type_pi="#header_process_optnc_iddocumenttype";
        this.selection_soumettre="#header_process_tclab_submitis";
        this.selection_zone="#header_process_optnc_zonelegal";
        this.selection_pays="#header_process_tclab_address1countryid";
        this.selection_pays_naissance="#header_process_optnc_countryofbirth";
        this.selection_recherche_localite="#header_process_tclab_address1cityid";
        this.selection_codeP="#header_process_optnc_address1_lstpostalcode";
        this.selection_factu_idem="#header_process_tclab_useprimaryaddressasbillingaddressis";

        this.liste_pays=By.xpath("//ul[@id='header_process_tclab_address1countryid_i_IMenu']");
        this.liste_pays_naissance=By.xpath("//ul[@id='header_process_optnc_countryofbirth_i_IMenu']");
        this.liste_localite=By.xpath("//ul[@id='header_process_tclab_address1cityid_i_IMenu']");
        this.liste_CP=By.xpath("//ul[@id='header_process_optnc_address1_lstpostalcode_i_IMenu']");
    }
}
