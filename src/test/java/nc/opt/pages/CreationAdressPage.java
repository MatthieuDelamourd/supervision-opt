package nc.opt.pages;

import nc.opt.library.Global;
import org.junit.Assert;
import org.openqa.selenium.By;

/**
 * Created by mdelamourd on 20/11/2017.
 */
public class CreationAdressPage extends Global{
    public String title;
    public By bouton_soumettre;
    public By type_adresse_selection;
    public By adresse_leg;
    public By adresse_factu;
    public By adresse_install;
    public By adresse_zone;
    public By zone_nc;
    public By refloc_champs_adresse;
    public By refloc_bouton_recherche;
    public By point_adresse;
    public By selection_desserte;
    public By choix_desserte;
    public By save_close;
    public By close;
    public By bouton_enregistrer;
    public By zoomIn;
    public By zoomOut;

    public CreationAdressPage() {
        this.title = "Adresse Client : Créer Adresse Client - Microsoft Dynamics CRM";
        this.bouton_soumettre = By.xpath("//*[@id=\"etel_customeraddressbusinessinteraction|NoRelationship|Form|etel.etel_customeraddressbusinessinteraction.Button1.Button\"]/span/a");
        this.type_adresse_selection = By.xpath("//*[@id=\"etel_customeraddresstypecode\"]/div");
        this.adresse_leg=By.cssSelector("#etel_customeraddresstypecode_i > option:nth-child(2)");
        this.adresse_factu=By.cssSelector("#etel_customeraddresstypecode_i > option:nth-child(3)");
        this.adresse_install=By.cssSelector("#etel_customeraddresstypecode_i > option:nth-child(4)");
        this.adresse_zone = By.xpath("//*[@id=\"optnc_region\"]/div[1]/span");
        this.zone_nc = By.cssSelector("#optnc_region_i > option:nth-child(2)");
        this.refloc_champs_adresse = By.xpath("//*[@id=\"search-input\"]");
        this.refloc_bouton_recherche = By.xpath("/html/body/div[3]/table/tbody/tr/td/div/div[1]/table/tbody/tr/td[4]/a");
        this.point_adresse = By.cssSelector("#results input");
        this.selection_desserte = By.cssSelector(".desserte-choice a span");
        this.zoomIn = By.cssSelector("div#mapDiv div:nth-child(2) div  a:nth-child(1)");
        this.zoomOut = By.cssSelector("div#mapDiv div:nth-child(2) div  a:nth-child(2)");
        this.choix_desserte = By.cssSelector("div#mapDiv div:nth-child(1) div:nth-child(6) div div:nth-child(2) div a");
        this.save_close = By.cssSelector(".refloc tr td:nth-child(6) a");
        this.close = By.cssSelector("#InlineDialogCloseLink");
      //  this.bouton_enregistrer = By.cssSelector("#footer_statuscontrol div:nth-child(2)");
        this.bouton_enregistrer = By.cssSelector("#savefooter_statuscontrol");
    }

}
