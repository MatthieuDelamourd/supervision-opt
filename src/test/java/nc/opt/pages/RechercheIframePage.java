package nc.opt.pages;

import nc.opt.library.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mdelamourd on 13/11/2017.
 */
public class RechercheIframePage extends Global {
    public Map<String, By> content;
    public String title;
    public By recherche_selection;
    public By recherche_type_msisdn;
    public By recherche_type_nom;
    public By recherche_type_phone;
    public By recherche_type_compte;
    public By champs_nom;
    public By champs_prenom;
    public By champs_raison_sociale;
    public By tableau_recherche_orga;
    public By tableau_recherche_prive;
    public By tableau_recherche_refcli;
    public By orga_clients;
    public By fiche_client_nom;

    public RechercheIframePage() {
        this.title = "Customer Search";
        fiche_client_nom = By.cssSelector("#FormTitle h1");
        recherche_selection = By.cssSelector("select#LookFor");
        recherche_type_msisdn = By.cssSelector(".tMSISDN");
        recherche_type_nom = By.cssSelector(".tName");
        recherche_type_phone = By.cssSelector(".tPhone");
        recherche_type_compte = By.cssSelector(".tAccountNumber");
        champs_nom = By.cssSelector("#SearchLastNameText");
        champs_prenom = By.cssSelector("#SearchText");
        champs_raison_sociale = By.cssSelector("#SearchText");
        tableau_recherche_prive =  By.xpath("//*[@id=\"SearchResultsGridIndDiv\"]/div/div/div[1]/div[2]/div[2]/table/tbody");
        tableau_recherche_orga =  By.xpath("//*[@id=\"SearchResultsGridCorpDiv\"]/div/div/div[1]/div[2]/div[2]/table/tbody");
        tableau_recherche_refcli = By.xpath("//*[@id=\"SearchResultsRefCliGridDiv\"]/div/div/div[1]/div[2]/div[2]/table/tbody");
        orga_clients = By.cssSelector("input[value=\"Corporates\"]");
    }
}
