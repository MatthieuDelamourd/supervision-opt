package nc.opt.pages;

import nc.opt.library.Global;
import org.openqa.selenium.By;

/**
 * Created by mdelamourd on 13/11/2017.
 */
public class VueClientPage extends Global {
    public String title;
    public By bouton_creer_adresse;
    public By bouton_ajout_contrat;
    public By liste_adresse;
    public By zone_adresse_install;
    public By zone_offre_drop;
    public By zone_nd_drop;
    public By premier_nd_liste;
    public By selection_vue;
    public By liste_vues;
    public By liste_souscription;
    public By liste_souscription_orga;
    public By liste_souscription_individual;
    public By selection_vue_360;
    public By selection_vue_factu;
    public By selection_vue_commandes;
    public By selection_vue_infos;
    public By vue_360_chargee;
    public By vue_factu_chargee;
    public By vue_commandes_chargee;
    public By vue_infos_chargee;
    public By sauter_rdv;
    public By confirmer;
    public By generer_contrat;
    public By workflow_btn1;
    public By workflow_btn2;
    public By workflow_btn3;
    public By workflow_btn4;
    public By workflow_btn5;
    public By workflow_btn6;
    public By workflow_btn7;
    public By recherche_souscription;
    public By accordeon_souscription_offre;


    public VueClientPage() {
        this.bouton_creer_adresse = By.xpath("//*[@id=\"contact|NoRelationship|Form|etel.contact.CreateCustomerAddress.Button\"]/span/a/span");
        this.bouton_ajout_contrat =  By.xpath("//*[@id=\"contact|NoRelationship|Form|optnc.contact.FixedActivation.Button\"] /span/a/span");
        this.liste_adresse = By.cssSelector(".search-accordion-tabs.eb_scrollbar span span");
        this.zone_adresse_install = By.cssSelector("#containerBody .drop-area");
        this.zone_offre_drop = By.cssSelector(".prepare-quote-data-container tree");
        this.zone_nd_drop = By.cssSelector(".drop-area");
        this.sauter_rdv = By.cssSelector(".skip-appointment div button");
        this.confirmer = By.cssSelector(".pull-right-dataActivation button:nth-child(4)");
        this.generer_contrat = By.cssSelector(".pull-right-dataActivation button:nth-child(3)");
        this.workflow_btn1 = By.cssSelector("#icon_container div:nth-child(2) flow-manager-step-icon div div");
        this.workflow_btn2 = By.cssSelector("#icon_container div:nth-child(3) flow-manager-step-icon div div");
        this.workflow_btn3 = By.cssSelector("#icon_container div:nth-child(4) flow-manager-step-icon div div");
        this.workflow_btn4 = By.cssSelector("#icon_container div:nth-child(5) flow-manager-step-icon div div");
        this.workflow_btn5 = By.cssSelector("#icon_container div:nth-child(6) flow-manager-step-icon div div");
        this.workflow_btn6 = By.cssSelector("#icon_container div:nth-child(7) flow-manager-step-icon div div");
        this.workflow_btn7 = By.cssSelector("#icon_container div:nth-child(8) flow-manager-step-icon div div");
        this.liste_souscription = By.cssSelector("#SubsriptionGrid_divDataArea table tbody tr:nth-child(1)");
        this.liste_souscription_orga = By.cssSelector("#SubsriptionGrid_divDataArea table tbody tr:nth-child(1)");
        this.liste_souscription_individual = By.cssSelector("#SubscriptionGrid_divDataArea table tbody tr:nth-child(1)");
        this.recherche_souscription = By.cssSelector(".search-form-button-area button");
        this.accordeon_souscription_offre = By.cssSelector("search-accordion-tree");
        this.premier_nd_liste = By.cssSelector(".search-accordion-tabs div:nth-child(1) div span:nth-child(2) span");

        this.selection_vue = By.cssSelector("#header_crmFormSelector");
        this.selection_vue_360 = By.cssSelector("");
        this.selection_vue_factu = By.cssSelector("");
        this.selection_vue_commandes = By.cssSelector("");
        this.selection_vue_infos = By.cssSelector("");
        this.vue_360_chargee = By.cssSelector("");
        this.vue_factu_chargee = By.cssSelector("");
        this.vue_commandes_chargee = By.cssSelector("");
        this.vue_infos_chargee = By.cssSelector("");
        this.liste_vues = By.cssSelector("#Dialog_0 li a:nth-child(2)");
    }
}
