package nc.opt.pages;

import nc.opt.library.Global;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.*;

/**
 * Created by mdelamourd on 13/11/2017.
 */
public class MainPage extends Global {
    //public Map<String, By> content;
    public String title;
    public By menu;
    public By champs_recherche;
    public By bouton_recherche;
    public By champs_utilisateur;
    public By creation_prive;

    public MainPage() {
        this.title = "Service : Recherche Client - Microsoft Dynamics CRM";
        this.menu = By.xpath("//*[@id='HomeTabLink']");
        this.champs_recherche = By.cssSelector("#search");
        this.bouton_recherche = By.cssSelector("#meqfSearchButton");
        this.champs_utilisateur = By.cssSelector(".navTabButtonUserInfoText.navTabButtonUserInfoWorker");
        this.creation_prive = By.cssSelector("#crmTopBar li:nth-child(1)");
    }
}
