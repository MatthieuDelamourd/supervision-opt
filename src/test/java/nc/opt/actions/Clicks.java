package nc.opt.actions;
import nc.opt.library.Global;
import org.aspectj.lang.annotation.DeclareAnnotation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by mdelamourd on 14/11/2017.
 */
public class Clicks extends Global {

    public void clickOnWebElement(WebElement el) {
        el.click();
    }

    //Drag and drop
    @Step
    public void dragAndDrop(WebElement origin, WebElement destination, WebDriver driver){
        (new Actions(driver)).dragAndDrop(origin, destination).perform();
    }

    //Clicks sur objet By
    @Step
    public void inClick_on(By cssSelector, WebDriver driver) throws Exception {
        WebDriverWait attente = new WebDriverWait(driver, 30);
        attente.until(ExpectedConditions.elementToBeClickable(cssSelector));
        driver.findElement(cssSelector).click();
    }

    @Step
    public void inClick_onWithSkip(By cssSelector, WebDriver driver) throws Exception {
        WebDriverWait attente = new WebDriverWait(driver, 30);
        attente.until(ExpectedConditions.visibilityOfElementLocated(cssSelector));
        try {
            driver.findElement(cssSelector).click();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Step
    public void inDoubleClick_on(By cssSelector, WebDriver driver) throws Exception {
        WebDriverWait attente = new WebDriverWait(driver, 30);
        attente.until(ExpectedConditions.elementToBeClickable(cssSelector));
        Actions action = new Actions(driver);
        action.doubleClick(driver.findElement(cssSelector)).perform();

    }

    @Step
    public void clickAndHold(By cssSelector, int holdTimeMs, WebDriver driver) throws InterruptedException {
        Actions action = new Actions(driver);
        action.clickAndHold(driver.findElement(cssSelector)).build().perform();
        Thread.sleep(holdTimeMs);
        action.moveToElement(driver.findElement(cssSelector)).release();
    }

    //Clicks sur WebElements
    @Step
    public void clickOnWebElement(WebElement el, WebDriver driver) {
        WebDriverWait attente = new WebDriverWait(driver, 30);
        attente.until(ExpectedConditions.elementToBeClickable(el));
        el.click();
        el.click();
    }

    @Step
    public void inDoubleClick_onWE(WebElement element, WebDriver driver) throws Exception {
        Actions action = new Actions(driver);
        action.doubleClick(element).perform();
    }

    //Clicks sur élements dans iframes
    @Step
    public void cilckElement_uIframe(By selector, String iframe, WebDriver driver) throws Exception {
        Sys system = new Sys();
        sys.switchOnFrame(iframe, driver);
        inClick_on(selector, driver);
        sys.switchMainFrame(driver);
    }


    //Selection menu déroulant
    @Step
    public void inSelectMenuDeroulant(By menuSelector, String selection, WebDriver driver) throws Exception {
        inClick_on(menuSelector, driver);
        WebElement menu = driver.findElement(menuSelector);
        for (WebElement option : menu.findElements(By.tagName("option"))){
            if (option.getText().equalsIgnoreCase(selection)) {
                option.click();
                break;
            }
        }
    }

    @Step
    public void inSelectMenuDeroulantMesures(By menuSelector, By listeSelector, String selection, WebDriver driver) throws Exception {
        int it = 0;
        while (driver.findElements(listeSelector).isEmpty() && it<3){
            inClick_on(menuSelector, driver);
            it = it +1;
        }
        if (driver.findElements(listeSelector).get(0).getText().equalsIgnoreCase("fiche client")) {
            for (WebElement a : driver.findElements(listeSelector)){
                if (a.getAttribute("title").equalsIgnoreCase(selection)) {
                    a.click();
                    break;
                }
            }
        } else {
            inSelectMenuDeroulantMesures(menuSelector, listeSelector, selection, driver);
        }
    }

    @Step
    public void inSelectMenuDeroulantFormulaire(String menuSelector, String selection, WebDriver driver) throws Exception {
        Sys s = new Sys();
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.cssSelector(menuSelector))).build().perform();
        s.pausems(1000);
        while(!driver.findElement(By.cssSelector(menuSelector)).findElement(By.tagName("option")).isDisplayed()) {
            inClick_on(By.cssSelector(menuSelector), driver);
            s.pausems(1000);
        }

        menuSelector = menuSelector + "_i";
        WebElement menu = driver.findElement(By.cssSelector(menuSelector));
        for (WebElement option : menu.findElements(By.tagName("option"))){
            if (option.getText().equalsIgnoreCase(selection)) {
                option.click();
            }
        }
    }

    public void inRechercheMenuDeroulantFormulaire(String menuSelector, String selection, By liste, WebDriver driver) throws Exception {
        Sys s = new Sys();
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.cssSelector(menuSelector))).build().perform();
        s.pausems(1000);
        inClick_on(By.cssSelector(menuSelector), driver);
        s.pausems(1000);
        WebElement l = driver.findElement(liste);
        for (WebElement el : l.findElements(By.xpath("//li/a[2]"))){
            if (el.getText().split("\n")[0].equalsIgnoreCase(selection)) {
                el.click();
            }
        }
    }
}
