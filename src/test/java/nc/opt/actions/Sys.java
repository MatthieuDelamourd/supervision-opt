package nc.opt.actions;


import org.json.JSONArray;
import org.json.JSONObject;
import nc.opt.library.Global;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mdelamourd on 15/11/2017.
 */
public class Sys extends Global {
    public void Sys() {}

    //Copié collé
    public void copyPaste(String text) {
        try {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(new StringSelection(""),new StringSelection(""));
            selection = new StringSelection(text);
            clipboard.setContents(selection,selection);
            robot = new Robot();
            robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
            robot.keyPress(java.awt.event.KeyEvent.VK_V);
            robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
            robot.keyRelease(java.awt.event.KeyEvent.VK_V);
        } catch (Exception e){
            print("test2");
            print(e);
        }
    }

    //Pause
    public void pausems(int tps) throws InterruptedException {
        Thread.sleep(tps);
    }

    //Switchs
    public void switchOnFrame(String frame, WebDriver wdr) {
        wdr.switchTo().frame(frame);
    }

    public void switchMainFrame(WebDriver wdr){
        wdr.switchTo().defaultContent();
    }

    public void switchBack(WebDriver wdr){
       wdr.switchTo().window(wdr.getWindowHandles().toString());
    }

    @Step
    public void getSecondWindow(WebDriver driver) {
        String curWin = driver.getWindowHandle();
        WebDriverWait w = new WebDriverWait(driver,30);
        w.until(ExpectedConditions.numberOfWindowsToBe(2));
        for(String win : driver.getWindowHandles()) {
            if (win.equals(curWin)) {
                System.out.println("Focus popup");
            } else {
                driver.switchTo().window(win);
            }
        }
    }

    //Logs et traces
    //Print
    public void print(Object o) {
        System.out.println(o);
    }

    //Add logs
    public void addLog(Lev lev, String log){
        switch (log) {
            case "DEBUG":
                logger.debug(log);
                break;
            case "INFO":
                logger.info(log);
                break;
            case "WARN":
                logger.warn(log);
                break;
            case "ERR":
                logger.error(log);
                break;
            case "TRACE":
                logger.trace(log);
                break;
            default:
        }
    }

    //Webservices
    public Map<String, String> getRefLocInfo(String num_adresse, String nom_voie, String ville) throws IOException {
        Map<String,String> reflocinfo = new HashMap<String, String>();

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {

        }

        String reflocid = "";
        String rue = "";
        BufferedReader rd;
        OutputStreamWriter wr;
        String jsonContent = "";
        int size = nom_voie.split(" ").length;
        for(String mot : nom_voie.split(" ")) {
            rue = rue + "+" + mot;
        }
       String wsURL = "https://carto-cvg.intranet.opt/arcgis/rest/services/refloc/GPServer/search/execute?search="+num_adresse+"+"+rue+"%0D%0A&type=&geometry=&nc=&max=&valide=&filter=&profil=&env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&returnTrueCurves=false&f=pjson";
       //System.out.println(wsURL);
        //String wsURL = "https://carto-cvg.intranet.opt/arcgis/rest/services/refloc/GPServer/search/execute?search="+rue+"%0D%0A&type=&geometry=&nc=&max=&valide=&filter=&profil=&env%3AoutSR=&env%3AprocessSR=&returnZ=false&returnM=false&returnTrueCurves=false&f=pjson";

        //JSONObject json = new JSONObject(IOUtils.toString(new URL(wsURL), Charset.forName("UTF-8")));
        try
        {
            URL url = new URL(wsURL);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(5000);
            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.flush();

            // Get the response
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                jsonContent = jsonContent + line;
            }
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
        JSONObject jsonObject = new JSONObject(jsonContent);
        JSONArray results  = (new JSONObject((jsonObject.getJSONArray("results").get(0)).toString())).getJSONArray("value");

        for ( Object val:results) {
            JSONObject desserte = new JSONObject(((JSONObject) val).get("attributes").toString());
            if(ville.equalsIgnoreCase(desserte.get("adresse_localite").toString().replace("-"," "))){
                reflocinfo.put("reflocid",desserte.get("id").toString());
                reflocinfo.put("refloc_adresse",desserte.get("adresse_point_adresse").toString());
                reflocinfo.put("refloc_ville",desserte.get("conofr").toString());
                reflocinfo.put("refloc_lieudit",desserte.get("adresse_lieu_dit").toString());
                reflocinfo.put("refloc_pays","NOUVELLE-CALEDONIE");
                reflocinfo.put("refloc_localite",desserte.get("adresse_localite").toString());
                reflocinfo.put("refloc_CP",desserte.get("lccp").toString());
                reflocinfo.put("refloc_province",desserte.get("pvnofr").toString());
                break;
            }
        }

        return reflocinfo;
    }
}
