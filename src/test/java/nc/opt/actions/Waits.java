package nc.opt.actions;

import nc.opt.library.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.round;

/**
 * Created by mdelamourd on 14/11/2017.
 */
public class Waits extends Global {
    public Waits(){}
    public Screen s;

    public void waitAndSkipAlert(Long timeoutMilliseconds,WebDriver driver) throws InterruptedException {
        int i = 0;
        while(i < round(timeoutMilliseconds/1000)+1) {
            try {
                driver.switchTo().alert();
                driver.switchTo().alert().accept();
                System.out.println("Popup détéctée et passée");
                break;
            } catch (Exception e) {
                Thread.sleep(1000);
                System.out.println("Attente popup : " + i + "secondes" );
                i++;
            }
        }
    }

    //Waits
    public boolean waitLogos_(String img) {
        //Chargement de TCRM
        ArrayList<String> imgs = new ArrayList<String>();
        boolean ret = false;
        Pattern re = Pattern.compile(img);
        Matcher matcher;
        File folder = new File("img");
        File[] listOfFiles = folder.listFiles();
        String sikuliTarget;
        s = new Screen();

        for (File f : listOfFiles) {
            if(re.matcher(f.getName()).find()) {
                sikuliTarget = "img/" + f.getName();
                try {
                    s.wait(sikuliTarget);
                    ret = true;
                } catch (FindFailed findFailed) {
                    findFailed.printStackTrace();
                }
            }
            if (ret == true) { break; }
        }

        if (ret == true) { return true; }
        else{ return false; }
    }
}
