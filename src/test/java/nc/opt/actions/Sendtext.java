package nc.opt.actions;

import nc.opt.library.Global;
import org.openqa.selenium.*;
import ru.yandex.qatools.allure.annotations.Step;

import java.awt.event.KeyEvent;

/**
 * Created by mdelamourd on 14/11/2017.
 */
public class Sendtext extends Global {
    public Sendtext(){}
    public Sys system;

    //Textes
    @Step
    public void sendSimpleText_selenium(String texte, WebElement el) {
        el.sendKeys(texte);
    }

    public void sendSimpleText_robot(String texte, WebElement el) {
        for( char c: texte.toCharArray()){
            robot.keyPress(KeyEvent.getExtendedKeyCodeForChar(c));
            robot.keyRelease(KeyEvent.getExtendedKeyCodeForChar(c));
        }
    }

    public void sendTextJS(String text, WebElement el, WebDriver driver) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", el,
                text);
    }

    public void sendSimpleText_sikuli(By cssSelector, String text, WebDriver driver) throws Exception {
        system = new Sys();
        try {
            if (driver.findElement(cssSelector).isDisplayed()) {
                driver.findElement(cssSelector).click();
                s.type(text);
                system.print("ACTION : Saisie du texte " + text + " dans -> " + cssSelector.toString());
            }
        } catch (Exception e){
            System.out.println("ERROR : L'element, "+cssSelector+" n'a pas ete trouve sur la page");
            driver.quit();
        }
    }

    //Keys
    public void pressKey_selenium(By cssSelector, Keys key, WebDriver driver) {
        driver.findElement(cssSelector).sendKeys(key);
    }

    public  void pressKey_robot(int key) {
        robot.keyPress(key);
        robot.keyRelease(key);
    }
}
