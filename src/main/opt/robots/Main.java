package opt.robots;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.util.Scanner;

/**
 * Created by mdelamourd on 22/11/2017.
 */
public class Main {
    public static int PRETTY_PRINT_INDENT_FACTOR = 4;
    public static String TEST_XML_STRING = "";
    public static String path = "C:\\Dev\\supervision-opt\\target\\allure-results";

    public static void main(String [] args) throws IOException, InterruptedException {
        File folder = new File(path);
        String[] files = folder.list();
        int i = 1;

        for (String file : files) {
            if(file.split(".")[1].equals("xml")){
                convertXmlToJson(path+"\\"+file, path+"\\"+"ResultXmlConvert"+i+"TO.json");
            }
            i++;
        }
    }

    public static void convertXmlToJson(String filepath, String output) throws IOException {
        try {
            TEST_XML_STRING = readFile(filepath);
            JSONObject xmlJSONObj = XML.toJSONObject(TEST_XML_STRING);
            String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
            writeFile(output, jsonPrettyPrintString);
        } catch (JSONException je) {
            System.out.println(je.toString());
        }
    }

    public static String readFile(String file) throws IOException {
        String text = "";

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        while ((line = reader.readLine()) != null) {
            text = text + "\n" + line;
        }
        return text;
    }

    public static void writeFile(String file, String texte) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter (new FileOutputStream(file), "utf-8"));
        writer.write(texte);
        writer.close();
    }


        /*Scanner s = new Scanner(System.in);
        System.out.print("$ ");
        String cmd = "cmd /c -gs settings.xml test -Dtest=nc.opt.selenium.mesures.MesureRechercheManuelle -Dtest.env=PREPROD -Dtest.user=CVGNAME001 -Dtest.browser=ie -Dtest.jdd=src/test/resources/jdd/jdd_cp3v2.csv -Dtest.capture=false -Dtest.no_submit=true";
        final Process p = Runtime.getRuntime().exec(cmd);

        new Thread(new Runnable() {
            public void run() {
                BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line = null;

                try {
                    while ((line = input.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        p.waitFor();*/
}
