package nc.opt.selenium.creation;

import nc.opt.library.TestParent;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;


/**
 * Created by mdelamourd on 26/10/2017.
 */
public class SouscriptionTest extends TestParent {

    @Test
    public void SouscriptionTest() throws Exception {
        Environnement(this.env);
        login();
        recherche_par_nom(this.nom_client, this.prenom_client, true);
        Boolean error = selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);

        String ND = ajout_contrat();
        driver.close();
        driver.switchTo().window(driver.getWindowHandles().toArray(new String[0])[0]);
        verifier_chargement_360();

        //Verification ligne active (jusqu'à 30mn!!!)
        driver.switchTo().defaultContent();
        selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);
        verification_ligne_active(ND);
    }

    @BeforeClass
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterClass
    public void tearDown() throws IOException { super.tearDown(); }
}
