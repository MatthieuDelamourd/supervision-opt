package nc.opt.selenium.creation;

import nc.opt.library.TestParent;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;
import java.io.IOException;


/**
 * Created by mdelamourd on 26/10/2017.
 */
@Features("Rattrapage")

public class CreationClientRattrapage extends TestParent {

    @Title("Création client")
    @Test()
    @Stories("Création fiche client")
    public void CreationClient() throws Exception {
        Environnement(this.env);
        recherche_par_nom(this.nom_client, this.prenom_client, false);
        selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);
    }
    @BeforeClass
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterClass
    public void tearDown() throws IOException { super.tearDown(); }
}
