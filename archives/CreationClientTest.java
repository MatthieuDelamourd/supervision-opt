package nc.opt.selenium.creation;

import nc.opt.library.TestParent;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.Title;

import java.awt.*;
import java.io.IOException;


/**
 * Created by mdelamourd on 26/10/2017.
 */

@Features("Mesure de Performance")

public class CreationClientTest extends TestParent {
    @Title("Création client - Ajout adresse")
    @Test()
    @Stories("Creation client")
    public void CreationClientTest() throws Exception {
        Environnement(this.env);
        login();
        recherche_par_nom(this.nom_client, this.prenom_client, true);
        Boolean error = selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);
        if (!error) {
            creationFormRefCli(this.type_client);
            Thread.sleep(2000);
            driver.switchTo().window(driver.getWindowHandles().toArray(new String[0])[0]);
            driver.close();
            driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
        }

        verifier_chargement_360();
        ajout_adresse_install(this.adresse_install);
        Thread.sleep(10000);
    }

    @BeforeClass
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterClass
    public void tearDown() throws IOException { super.tearDown(); }
}
