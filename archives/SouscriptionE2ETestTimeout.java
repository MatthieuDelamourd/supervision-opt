package nc.opt.selenium.creation;

import nc.opt.library.TestParent;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;


/**
 * Created by mdelamourd on 26/10/2017.
 */
public class SouscriptionE2ETestTimeout extends TestParent {

    @Test()
    public void SouscriptionE2ETest() throws Exception {
        Environnement(this.env);
        //long startCreation = System.currentTimeMillis();
        login();
        this.nom_client = "BANUKONE";
        this.prenom_client = "JEAN";
        this.date_naissance = "12/05/1978";

        recherche_par_nom(this.nom_client, this.prenom_client, true);
        Boolean error = selection_client_creation(this.nom_client, this.prenom_client, this.date_naissance);
        //if (!error) {
        //    creationFormRefCli(this.type_client);
        //    Thread.sleep(2000);
        //    driver.switchTo().window(driver.getWindowHandles().toArray(new String[0])[0]);
        //    driver.close();
        //    driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
        //}

        verifier_chargement_360();
        //ajout_adresse_install(this.adresse_install);
        //long endCreation = System.currentTimeMillis();
        //Thread.sleep(10000);
        long startContrat = System.currentTimeMillis();
        String ND = ajout_contrat();
        driver.close();
        driver.switchTo().window(driver.getWindowHandles().toArray(new String[0])[0]);
        verifier_chargement_360();
        long endContrat = System.currentTimeMillis();

        //Verification ligne active (jusqu'à 30mn!!!)
        //long startActivation = System.currentTimeMillis();
        //driver.switchTo().defaultContent();

        //verification_ligne_active(ND);
        //long endActivation = System.currentTimeMillis();

        //affichageMesure("Durée création client : " + String.valueOf(Float.valueOf(endCreation - startCreation)/1000), 3600000 );
        //affichageMesure("Durée souscription : " + String.valueOf(Float.valueOf(endContrat - startContrat)/1000), 3600000 );
        //affichageMesure("Durée activation ligne : " + String.valueOf(Float.valueOf(endActivation - startActivation)/1000), 3600000 );
        //affichageMesure("Durée total cycle sans attente : " + String.valueOf(Float.valueOf(endCreation - startCreation + endContrat - startContrat + endActivation - startActivation)/1000), 3600000 );
    }
    @BeforeClass
    public void setUp() throws IOException, AWTException { super.setUp(); }

    @AfterClass
    public void tearDown() throws IOException { super.tearDown(); }
}
