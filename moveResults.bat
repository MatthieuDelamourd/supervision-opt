@echo off
if not exist C:\Dev\RESULTAT mkdir C:\Dev\RESULTAT
if not exist C:\xampp\htdocs\RESULTAT mkdir C:\xampp\htdocs\RESULTAT

set str_date=%date%
set str_date=%str_date:/=%
set folder=C:\Dev\RESULTAT\%str_date%
set folder2=C:\xampp\htdocs\RESULTAT\%str_date%
if not exist %folder% mkdir %folder%
if not exist %folder2% mkdir %folder2%

for /F "usebackq tokens=1,2 delims==" %%i in (`wmic os get LocalDateTime /VALUE 2^>NUL`) do if '.%%i.'=='.LocalDateTime.' set ldt=%%j
set ldt=%ldt:~0,4%-%ldt:~4,2%-%ldt:~6,2% %ldt:~8,2%:%ldt:~10,2%:%ldt:~12,6%
set str_time=%ldt%
set str_time=%str_time:~-12%
set str_time=%str_time:~0,8%
set str_time=%str_time::=%
set folder=C:\Dev\RESULTAT\%str_date%\%str_time%
set folder2=C:\xampp\htdocs\RESULTAT\%str_date%\%str_time%

xcopy "c:\Dev\supervision-opt\target\allure-results" "%folder%" /e /i /y /s
xcopy "c:\Dev\supervision-opt\target\site" "%folder%\report" /e /i /y /s
xcopy "c:\Dev\supervision-opt\target\videoCap" "%folder%\video" /e /i /y /s
xcopy "c:\Dev\supervision-opt\target\allure-results" "%folder2%" /e /i /y /s
xcopy "c:\Dev\supervision-opt\target\site" "%folder2%\report" /e /i /y /s
xcopy "c:\Dev\supervision-opt\target\videoCap" "%folder2%\video" /e /i /y /s

